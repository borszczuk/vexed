#!/usr/bin/python -ui

from random import *

seed(1435)

def GenPuzzle():

    result = [ [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 ] ]



    ## Place 9
    i = randint(4,20)

    for j in range(0,i):

        row = randint(1,6)
        col = randint(1,8)
        result[row][col] = 9

    ## place remaining
    h = randint(6, 38-i)

    trials = 1000
    
    while (trials > 0):

        ## Decide between valid move and valid place 2
        if random() > 0.75:
            if ValidPlace(result):
                h = h -2
                print "Place 2 OK"
                ValidPuzzle(result)
            ##se:
                ##int "Place 2 Fail"
        else:
            if ValidMove(result):
                print "Move OK"
                ValidPuzzle(result)
            ##se:
                ##int "Move Fail"

        trials = trials - 1

    return result

def Move(result,row,col):

    pick = randint(0,2)

    if pick == 0:
        return LeftMove(result,row,col)
    if pick == 1:
        return RightMove(result,row,col)
    else:
        return UpMove(result,row,col)

def LeftMove(result,row,col):

    if col==1:
        return 0

    newcol = randint(1,col-1)

    ## Check if clear
    for j in range(newcol,col):
        if not (result[row][j] == 0):
            return 0

    ## Check if bottom support
    for j in range(newcol,col):
        if result[row-1][j] == 0:
            return 0

        if result[row-1][j] == result[row][col]:
            return 0

    ## Check if valid position
    if ValidPosition(result,row,newcol,result[row][col]):
        result[row][newcol] = result[row][col]
        result[row][col] = 0
        return 1
    else:
        return 0

def RightMove(result,row,col):

    if col==8:
        return 0

    newcol = randint(col+1,8)

    ## Check if clear
    for j in range(col+1,newcol+1):
        if not (result[row][j] == 0):
            return 0

    ## Check if bottom support
    for j in range(col+1,newcol+1):
        if result[row-1][j] == 0:
            return 0

        if result[row-1][j] == result[row][col]:
            return 0

    ## Check if valid position
    valid = ValidPosition(result,row,newcol,result[row][col])

    if valid:
        result[row][newcol] = result[row][col]
        result[row][col] = 0
        return 1
    else:
        return 0

def UpMove(result,row,col):

    if row == 6:
        return 0

    newrow = randint(row+1,6)

    ## Check if clear
    for j in range(row+1,newrow+1):
        if not (result[j][col] == 0):
            return 0

    ## Check if valid place to right or left
    offset = randint(1,2)
    if offset == 2:
        offset = -1

    ## Check if valid position
    valid = ValidPosition(result,newrow,col+offset,result[row][col])

    if valid:
        result[newrow][col+offset] = result[row][col]
        result[row][col] = 0
        return 1
    else:
        return 0

def ValidPosition(result,row,col,entry):

    ## Must be empty
    if not result[row][col] == 0:
        #print "not empty"
        return 0

    ## Must have support
    if result[row-1][col] == 0:
        #print "no support"
        return 0

    ## Must not be ontop of self
    if result[row-1][col] == entry:
        #print "on top of self"
        return 0

    ## Must not be by self
    try:
        if result[row][col-1] == entry:
            #print "right of self"
            return 0
    except IndexError:
        pass

    ## Must not be by self
    try:
        if result[row][col+1] == entry:
            #print "left of self"
            return 0
    except IndexError:
        pass

    return 1

def Place2(result,row,col):

    ## random entry
    entry = randint(1,8)

    ## Can't place on top of self
    if result[row-1][col] == entry:
        return 0

    ## Left or right
    offset = randint(1,2)
    if offset == 2:
        offset = -1

    ## Can't place on top of self
    if result[row-1][col+offset] == entry:
        return 0

    ## Can't place if spot not clear
    if not result[row][col+offset] == 0:
        return 0

    ## Can't place if no support
    if result[row-1][col+offset] == 0:
        return 0

    ## Can't place by self
    try:
        if result[row][col-offset] == entry:
            return 0
    except IndexError:
        pass

    try:
        if result[row][col+offset+offset] == entry:
            return 0
    except IndexError:
        pass

    ## OK to place
    result[row][col] = entry
    result[row][col+offset] = entry

    ## Do a move
    pick = randint(0,1)
    if pick == 0:
        ncol = col
    else:
        ncol = col+offset

    if not Move(result,row,ncol):
        result[row][col] = 0
        result[row][col+offset] = 0
        return 0

    return 1


def Place3_1(result,row,col):

    ## random entry
    entry = randint(1,8)

    ## Can't place on top of self
    if result[row-1][col] == entry:
        return 0

    ## Can't place by self if one up
    if result[row+1][col+1] == entry:
        return 0

    if result[row+1][col-1] == entry:
        return 0

    ## Can't place if occupied
    if not result[row+1][col] == 0:
        return 0

    ## Left or right
    offset = randint(1,2)
    if offset == 2:
        offset = -1

    ## Can't place on top of self
    if result[row][col+offset] == entry:
        return 0

    ## Can't place if spot not clear
    if not result[row+1][col+offset] == 0:
        return 0

    ## Can't place if no support
    if result[row][col+offset] == 0:
        return 0

    ## Can't place by self
    try:
        if result[row+1][col+offset+offset] == entry:
            return 0
    except IndexError:
        pass

    ## OK to place
    result[row][col] = entry
    result[row+1][col] = entry
    result[row+1][col+offset] = entry

    if not Move(result,row+1,col):
        result[row][col] = 0
        result[row+1][col] = 0
        result[row+1][col+offset] = 0
        return 0

    return 1

def ValidSpace(result,row,col):

    if not (result[row][col] == 0):
        return 0

    if result[row-1][col] == 0:
        return 0

    return 1

def ValidPiece(result,row,col):

    if result[row][col] == 0:
        return 0;

    if result[row][col] == 9:
        return 0;

    if (result[row+1][col] == 0) or (result[row+1][col] == 9):
        return 1;

    return 0

def ValidPlace(result):

    for h in range(0,50):
        row = randint(1,6)
        col = randint(1,8)
        valid = ValidSpace(result,row,col)

        if valid:
            trial = randint(0,1)
            if trial == 0:
                valid = Place2(result,row,col)
            else:
                valid = Place3_1(result,row,col)

        if valid:
            return 1

    return 0

def ValidMove(result):

    for h in range(0,50):
        row = randint(1,6)
        col = randint(1,8)
        valid = ValidPiece(result,row,col)

        if valid:
            valid = Move(result,row,col)

        if valid:
            return 1

    return 0

def ValidPuzzle(result):

    for row in range(1,7):

        for col in range(1,9):

            if not ((result[row][col] == 0) or (result[row][col] == 9)):

                    ## Must be supported
                if (result[row-1][col] == 0):
                    print "Bad Support!"

                if (result[row-1][col] == result[row][col]):
                    print "On top of same kind!"

                if (result[row][col-1] == result[row][col]):
                    print "Left of same kind!"

    
outfile = open('puzzle.txt', 'w')

outfile.write("{\n")

for i in range(0,59):

    print "Generating puzzle",i

    outfile.write("\t{\n")

    result = GenPuzzle()
    result.reverse()

    k = 0
    for row in result:
         outfile.write("\t\t{")
         for j in range(0,len(row)):

             if j < len(row)-1:
                 str = repr(row[j]) + ", "
             else:
                 str = repr(row[j])
             outfile.write(str)
    
         if k < len(result)-1:
             outfile.write("},\n")
         else:
             outfile.write("}\n")

         k = k + 1

    if i < 58:
        outfile.write("\t},\n")
    else:
        outfile.write("\t}\n")

outfile.write("}\n")

outfile.close()

        
