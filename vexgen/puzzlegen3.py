#!/usr/bin/python -ui

from random import *

seed(2000)

def GenPuzzle():

    result = [ [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 9 ],
               [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 ] ]

    placed = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]

    ## Place 9s
    i = randint(4,10)

    while i > 0:
        row = randint(1,6)
        col = randint(1,8)
        result[row][col] = 9

        if ReachSpace(result):
            result[row][col] = 0
        else:
            i = i - 1
           
    ## place remaining
    h = randint(6, 32-i)

    for j in range(0,h):

        ## Find empty and valid place
        valid = 0
        while not valid:
            row = randint(1,6)
            col = randint(1,8)
            entry = randint(1,8)

            valid = CheckValid(result,row,col,entry)

        result[row][col] = entry

        placed[entry] = placed[entry] + 1

    ## Make sure there is always more than one
    for j in range(0,len(placed)):

        if placed[j] == 1:
            ## Find empty and valid place
            valid = 0
            while not valid:
                row = randint(1,6)
                col = randint(1,8)
                entry = j

                valid = CheckValid(result,row,col,entry)

            result[row][col] = entry

    return result

def CheckValid(result,row,col,entry):

    if not (result[row][col] == 0):
        return 0

    if result[row-1][col] == 0:
        return 0

    if result[row-1][col] == entry:
        return 0

    if result[row][col-1] == entry:
        return 0

    if result[row][col+1] == entry:
        return 0

    return 1

def ReachSpace(result):

    ## Mark first empty space
    marked = 0
    for row in range(1,7):
        for col in range(1,9):
            if result[row][col] == 0:
                result[row][col] = 10
                marked = 1
                break
        if marked:
            break

    ## Reachable space
    fixed = 0
    while not fixed:
        fixed = 1
        for row in range(1,7):
            for col in range(1,9):
                if result[row][col] == 10:
                    if result[row+1][col] == 0:
                        result[row+1][col] = 10
                        fixed = 0
                    if result[row-1][col] == 0:
                        result[row-1][col] = 10
                        fixed = 0
                    if result[row][col+1] == 0:
                        result[row][col+1] = 10
                        fixed = 0                        
                    if result[row][col-1] == 0:
                        result[row][col-1] = 10
                        fixed = 0

    ## Check if partitioned
    partitioned = 0
    for row in range(1,7):
        for col in range(1,9):
            if result[row][col] == 0:
                partitioned = 1
            if result[row][col] == 10:
                result[row][col] = 0

    return partitioned
                
                        

    
outfile = open('puzzle.txt', 'w')

outfile.write("{\n")

for i in range(0,59):

    print "Generating puzzle",i

    outfile.write("\t{\n")

    result = GenPuzzle()
    result.reverse()

    k = 0
    for row in result:
         outfile.write("\t\t{")
         for j in range(0,len(row)):

             if j < len(row)-1:
                 str = repr(row[j]) + ", "
             else:
                 str = repr(row[j])
             outfile.write(str)
    
         if k < len(result)-1:
             outfile.write("},\n")
         else:
             outfile.write("}\n")

         k = k + 1

    if i < 58:
        outfile.write("\t},\n")
    else:
        outfile.write("\t}\n")

outfile.write("}\n")

outfile.close()

        
