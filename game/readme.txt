Vexed

Contents

0. What's new?
1. Introduction
2. Installation
3. Instructions
        Basic Game Play 
        Scoring 
        Undo, Memorize, Recall, Replay, Restart, Block Check
        Game Options 
        Level Packs 
        Solutions 
        Menus 
        Screen Layout 

4. Other info
5. Contact us
6. Known bugs / Todo's

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
      Revised by Marcin Orlowski <carlos@wfmh.org.pl> at 2006.04.11
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

0. What's new in this release of Vexed

   Please read changes.txt for list of changes introduced in this version


1. Introduction

        Welcome to Vexed!  Vexed is a great puzzle game for Palm OS 
        devices written by James McCombe.  He released the game and its 
        source code in 1999 under the GNU General Public License.  
        
        Now there's a group of people working together to improve Vexed as 
        an Open Source project on SourceForge.net.  Visit the Vexed 
        SourceForge project page at http://sourceforge.net/projects/vexed
        for more information.  

--------------------------------------------------------------------------------
        
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

2. Installation

        To install, simply install Vexed version using your Palm 
        install tool of choice.  You may safely install over previous 
        versions of Vexed.  
        
        The program and all external level packs are contained in the
        distribution archive file.

        You should be aware that since release 2.1, Vexed require at
        least one level pack to be installed as well. There's no 
        built-in levels any longer. If you beam Vexed to another device,
        remember to beam some level packs as well (the level selection
        dialog feature Beam button for that purpose).

        All level packs are external databases and should be installed via
        any Palm install tool.  Support for beaming  individual level packs
        is available from the Level Packs screen in Vexed.  
        
        See the Instructions section below for a description of the level
        packs.
        
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

3. Instructions 

Basic Game Play

        Vexed is a puzzle game in which your goal is to move similar 
        blocks together, which causes them to disappear.  Once all the 
        blocks are gone, you've solved the level and you're presented with 
        a new level to solve.  

        You move blocks by putting the stylus on them and dragging to 
        either the right or left.  If an empty space is encountered, 
        "gravity" takes over and the blocks fall towards the bottom.  

        You can move more than one block left or right with a single 
        drag of the stylus.  The block move stops on the first block in 
        which an action can occur, or where you ended dragging the stylus.  

	New with Version 2.1 is a "tap" method of moving blocks in 
	addition to dragging.  Use your stylus to tap the block you wish
	to move.  Then tap the open space where you want the block to move
	to.  The destination must be an open space to be valid.  The tap
	method lets you tap on a row different from the block.
	
Scoring

        The Vexed scoring is based on a golf-like "par" concept.  The 
        level pack has a solution stored for each level.  If you solve the 
        level in the same number of moves as what is stored in the level 
        pack, you get a score of 0, or par, for that level.  Note that the 
        stored solution may not be the most efficient one, so you can beat 
        "par" and have a negative score.  

        The score displayed in the title bar is a cumulative score for the 
        entire level pack.  

        Using the Solution feature adds five to your score.  

Undo, Memorize, Recall, Replay, Restart, Block Check

Undo 

        There is a 15-move undo facility.  Once you've made a move a "U" 
        icon appears at the bottom of the screen.  Tapping it will undo the 
        previous move.  The buffer holds 15 moves.  The undo buffer is not 
        saved across invocations of Vexed.

Memorize and Recall 

        Solving a difficult level might take many attempts involving many 
        moves.  The memorize feature lets you save where you are in a 
        level, and restart from that point.  Tap the "M" icon at the 
        bottom of the screen, and an "R" icon appears.  Tap the "R" 
        (recall) icon to restore the game board to its status when you 
        memorized it.  

Level Replay 

        You can replay all of the moves made in the current level, and 
        also of the previous level.  These options are under the "Game" 
        menu.

        Note!  The replay functions use the memorize buffer described 
        above.  If you replay a level, your current memorize buffer will 
        be emptied.  

Restart 

        Tap the restart icon at the bottom right of the screen to restart 
        a level from the beginning.  

        Note!  The Undo, Memorize, and Replay data is not saved when you 
        exit Vexed!  

Block Check 

        You can display just one type of blocks in order to help you solve
        the puzzle.  Hold the stylus down on a block while pressing the page 
        up key, and only blocks of that type will be displayed.  Once you 
        lift the stylus, the game board is redrawn.  

Game Options

        The preferences options you set in the preferences menu option are 
        saved in something called the "saved preferences." This data is 
        also saved back to your PC when you hotsync, so if you do a hard 
        reset, you'll be able to restore this information.  

        The state of the current level is also saved in preferences, but 
        this time in the "unsaved preferences." 

        This means that when you re-invoke Vexed, it'll go to your most 
        recent level and you won't start the level from the beginning.  
        Instead, it will go to the point at which you left off.  But this 
        data is not included with your hotsync to your PC, so you'll have 
        to restart the level from the beginning in the event of a hard 
        reset / restore sequence.  

Preferences

        You can set the following settings affecting how the program works 
        in the preferences screen found in the Game menu (or the "/R" 
        shortcut): 

Piece move animation 

        This animates (and slows down) the movement of blocks as they move 
        sideways.  Unselect this to speed up game play.  

Gravity animation 

        This animates (and slows down) the movement of blocks as they fall 
        due to gravity.  Unselect this to speed up game play.  

Piece elimination animation 
                              
        This animates (and slows down) the erasing of blocks as they 
        disappear.  Unselect this to speed up game play.  

Blinds Effect 

        This is the animation effect you see when a level is first loaded 
        or reloaded.  Unselect this to speed up game play.  

Skip Intro 

        Select this option to skip the animated introduction each time you 
        run Vexed.  You can also tap the screen or press the page up / 
        page down keys to interrupt the animated introduction.

Sound 

        Select this option to turn on sound effects.  

Block Set 

        This is a pop-up menu to let you select between 3 monochrome or 6 
        different block sets.  

        Note!  If you turn off the first three animations, the replay and 
        solution functions will be very fast, so you probably need to keep 
        at least one of them on if you use Solution or Replay.

Level Packs

        There are many different level packs available. Use the Level Packs 
        option from the Levels Menu to work with level packs.  The level 
        packs screen will display which level packs you have installed 
        on your device.  For each level pack it also shows your current 
        score, the highest level you've solved, and how many levels are in 
        the level pack, for example: 
        
                Confusion Pack:10 (4/60)

        The level packs screen also gives you information about the source 
        of the level pack and types of levels found within.  

        These are the current level packs: 

        Classic         Original Vexed 1.3 levels 
        
        Classic II      First expansion pack from Steve Haynal, 
                        previously released as Vexed 1.4a. 
                        
        Children's      Very easy levels meant for children. 
        
        Variety I       These levels provide a full spectrum of difficulties 
                        from easy to hard. 
                        
        Variety II      Continuation of the Variety Pack. From easy to hard. 

        Twister         Difficult. Many of these levels have twists that add 
                        challenge. 
                        
        Confusion Pack  Hard! These levels are tricky and require careful 
                        planning to solve. 
                        
        Panic           Very hard! So hard you'll panic after the first one. 
        
        Impossible      Impossible. You need to be Einstein to make it through 
                        this game pack! 
	                        
Solutions

        You can elect to see the solution for a level.  Select the Show 
        Solution option from the Levels Menu to view the solution.  Viewing 
        solutions adds 5 to your score!

        Selecting the Show Solution option saves your current level state 
        and the solution screen is drawn.  It automatically starts 
        displaying the solution and will continue unless you tap the 
        screen or press the page up / page down keys.  If you interrupt 
        it, use the arrow icons at the bottom of the screen or the page up 
        / page down keys to step forward and back through the solution.  

        Tapping the restart icon in the bottom right corner of the screen 
        restarts the solution from the beginning.  

        Tap the Done button to exit the solution and return to the level 
        where you left off.  

Menus

        Tapping on the title bar will open up the two pull down menus 
        available, Game and Levels.

Game Menu 

        The Game Menu has the following options: 

Preferences 

        Use this option to set the Game Options described above.  They are 
        stored in the Palm's "saved preferences." 

Replay Current Level 

        Show all the moves made so far in the current level from the 
        beginning.  

Replay Previous Level 

        Show all moves made in the previous level.  If you haven't solved 
        a level yet in your current session this option is not available.  

Beam Vexed 

        This menu option lets you beam the Vexed program to another 
        device.  Go into the Level Packs menu form to beam individual 
        level packs.  

How To Play 

        This displays some brief instructions on how to play the game.  

About 

        Displays information about Vexed.  Tap the Credits button for 
        information about the authors of Vexed.  

Levels Menu 

        The Levels Menu has the following options: 

Level Packs

        This option opens up a screen to deal with level packs.  Move the 
        cursor up and down through the list to see information about each 
        level, including the author and a description of the level pack.  

        This is the format of the level packs in the list: 

        "<name>:<level pack score> (<highest level completed> / <number of 
        levels in the pack>)

        For example, 

                "Confusion Pack: 10 (4/60)" 

        To load a level pack, move the cursor to the pack desired and tap 
        on OK.  

        You can beam level packs to another device by selecting the pack 
        to beam with the cursor and tapping on Beam.  

        You can delete a level pack by moving the cursor to the level pack 
        you wish to delete and tapping on Delete.  

Blocks Remaining 

        This option displays how many blocks there are remaining of each 
        type.  

Clear Solved Levels 

        The program remembers which level you were playing and how many 
        levels were solved when the program exits.  Use this option if you 
        want to reset this and have the program think you're starting over 
        on the very first level.  

Show Solution 

        Select this option to see how to solve the current level as 
        described above.  Viewing the solution will add 5 to your score.  

Screen Layout

        Refer to the icon at the top left of this page for an example of 
        the Vexed screen.  

Title Bar
 
        This is the format of the information on the title bar: 


Level number 

        The number of the level within the level pack, starting with 
        level 0.  

Level name 

        Each level pack has a theme used to assign names to individual 
        levels.  For example, the Children's Pack uses ice cream flavors.  
        It's more fun to be stuck on level Chocolate Chip than level 23, 
        isn't it?  If you wish to modify the level names there is a tool 
        to add titles to the level pack databases within the source code 
        zip file.  

Current number of moves 

        How many moves you've taken so far in the level.  

Par moves
 
        The number of moves for this level in the solution stored in the 
        level pack ("par").  Note that the solution stored may not be the
        most efficient, so it is possible to beat par and get a negative
        score.

Score 

        Your total score for the entire level pack.  It is the sum of your 
        moves above or below par for each level completed.  The lower the 
        number, the better your score.  


Gameboard
 
        Under the title bar is the game play area, a grid of blocks 10 by 
        8.  

Control Icons 

        Underneath the game board are a set of control icons.  From left 
        to right, here are their functions: 

First 

        The left-most icon jumps to the first level in the level pack, 
        level 0.  

Previous
 
        The second icon with the arrow pointing to the left moves to the 
        previous level.  If you're on level 0 this does nothing.  

Level Select 

        The magnifiying glass icon brings up a form to let you select 
        exactly which level you want to select.  Use the up and down 
        arrows or the page up / page down keys to select the new level.  
        It will display all levels in the level pack, but you cannot 
        select a level higher than one past the highest level you've 
        solved.  

Next 

        The right-hand pointing arrow will move up to the next level past 
        your current level.  You are only allowed to move to levels up to 
        one past the highest level you've solved.  

Last 

        The fifth icon jumps to the one past the highest level you've 
        solved.  

Undo 

        The sixth icon from the left is an uppercase "U" character.  This 
        icon appears after you've made your first move in a level.  
        Tapping this icon moves back one move.  The buffer for the undo 
        function holds 15 moves, so you may only go back that many moves.  

Recall 

        The seventh icon from the left is an uppercase "R" character.  It 
        appears when you tap the "Memorize" icon, discussed next.  Tap 
        the Recall icon to return the gameboard to the state it was in 
        when you memorized it.  

Memorize
 
        The eighth icon from the left is an uppercase "M" character.  Use 
        this function to memorize the current state of the gameboard in 
        order to return to it later.  

Restart 

        The icon in the bottom right erases all moves taken in the current 
        level and lets you start the level over from the beginning.  
 
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

4. Other Info

        Vexed Home Page                     http://vexed.sourceforge.net

        Vexed Project on SourceForge        http://sourceforge.net/projects/vexed

        Vexed Users Mailing List            http://lists.sourceforge.net/lists/listinfo/vexed-users  

        Vexed Developers Mailing List       http://lists.sourceforge.net/lists/listinfo/vexed-developers  

        Vexed Announcements Mailing List    http://lists.sourceforge.net/lists/listinfo/vexed-announce  

        Vexed CVS Source Tree               http://cvs.sourceforge.net/cgi-bin/cvsweb.cgi/?cvsroot=vexed  
 
        Scott Ludwig's Home Page            http://www.tinyware.com  

        Matthew McClintock's Home Page      http://work.colum.edu/~matma/  

        Steve Haynal's Vexed Page           http://softerhardware.com/vexed.html  

        Irked - Windows/PocketPC Vexed      http://www.ipidooma.net/irked/  

        Vexed for Psion Computers           http://www.freepoc.de/vexed.htm  

        If you are interested in making your own Vexed level packs, or 
        perhaps a Vexed level editor, please send a note to Scott Ludwig 
        (scottlu@eskimo.com).  There are now tools to generate and solve 
        levels, but it takes some horsepower and time to do it.  You can 
        get more information about the tools by downloading the Vexed 
        source code from the Vexed SourceForge project page, and then 
        Scott will help you to get started.  

        Also, there is a tool called "titler" which can add the level 
        titles seen at the top of each level.  You can run this tool to 
        create your own level titles.

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

5. Contact us

        You can subscribe to Vexed announce, users, and beta mailing 
        lists, and participate in discussions about Vexed in the forums
        at the Vexed project on SourceForge.net.  See the links above.
        
        You can also send mail to members of the Vexed team listed on 
        the Vexed Home Page or in program's "About" dialog.

6. Known bugs / Todo's

	- VFS support should be added
	- The tap from the about screen can be passed into the program
	  and taken as a move
