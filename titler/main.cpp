/*  main.c "puts titles in Vexed level files"
    Copyright (C) 2001 Scott Ludwig (scottlu@eskimo.com)
    
    This file is part of Vexed.

    Vexed is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Vexed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vexed; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Puts titles on vexed .ini level packs
// Copyright 2001, Scott Ludwig

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ini.h"

int gcTitles;
char **gaszTitles;
bool *gafTitleUsed;

dword HashString(char *psz)
{
	dword hash = 0;
	while (*psz != 0)
		hash = (hash << 5) + hash + *psz++;
	return hash;
}

bool ReadTitles(char *pszFnTitles)
{
	FILE *pf = fopen(pszFnTitles, "r");
	if (pf == NULL)
		return false;

	char sz[1024];
	int cTitles = 0;
	while (true) {
		if (fgets(sz, sizeof(sz) - 1, pf) == NULL)
			break;
		cTitles++;
	}
	if (cTitles == 0) {
		fclose(pf);
		return false;
	}

	gaszTitles = new char *[cTitles];
	memset(gaszTitles, 0, sizeof(char *) * cTitles);
	gafTitleUsed = new bool[cTitles];
	memset(gafTitleUsed, 0, sizeof(bool) * cTitles);

	fseek(pf, 0, SEEK_SET);
	gcTitles = 0;
	while (true) {
		if (fgets(sz, sizeof(sz) - 1, pf) == NULL)
			break;
		int cch = strlen(sz);
		int n;
		for (n = 0; n < cch; n++) {
			if (sz[n] == '\n') {
				sz[n] = 0;
				break;
			}
		}

		// No empty lines

		if (sz[0] == 0)
			continue;
		
		// No dupes

		bool fFound = false;
		for (n = 0; n < gcTitles; n++) {
			if (stricmp(sz, gaszTitles[n]) == 0) {
				fFound = true;
				break;
			}
		}
		if (fFound)
			continue;

		// Upper case words

		bool fUpper = true;
		for (n = 0; n < cch; n++) {
			if (fUpper) {
				fUpper = false;
				sz[n] = (char)CharUpper((LPTSTR)(dword)(byte)sz[n]);
			}
			if (sz[n] == ' ')
				fUpper = true;
		}
		gaszTitles[gcTitles++] = strdup(sz);
	}

	fclose(pf);
	srand(HashString(pszFnTitles));
	return true;
}

int RandInt(int a, int b)
{
	return (rand() % ((b - a) + 1)) + a;
}

void GetRandomTitle(char *psz, int cchMax)
{
	int n;
	while (true) {
		n = RandInt(0, gcTitles - 1);
		if (gafTitleUsed[n])
			continue;
		if (strlen(gaszTitles[n]) > (unsigned int)cchMax)
			continue;
		break;
	}
	gafTitleUsed[n] = true;
	strcpy(psz, gaszTitles[n]);
}

void ExitUsage()
{
	printf("Takes titles in random order from a list and puts them on vexed levels\n");
	printf("Usage: titler countChars titles.txt levelpack.ini levelpacknew.ini\n");
	printf("countChars: accept titles this long and under\n");
	printf("titles.txt: filename of text file with titles\n");
	printf("levelpack.ini: input level pack filename\n");
	printf("levelpacknew.ini: output level pack filename\n");
	printf("Note: this tool accepts files with duplicates and only assigns unique\n");
	printf("      names. It also capitalizes the first character of words\n");
	exit(1);
}

int main(int argc, char **argv)
{
	if (argc != 5)
		ExitUsage();
	int cchMax = atoi(argv[1]);
	char *pszFnTitles = argv[2];
	char *pszFnIni = argv[3];
	char *pszFnIniNew = argv[4];

	// First try to read the .ini

	int cSections;
	IniSection *psec = LoadIniFile(pszFnIni, &cSections);
	if (psec == NULL) {
		printf("Error parsing .ini file %s.\n", pszFnIni);
		exit(1);
	}

	// Now try to read the titles

	if (!ReadTitles(pszFnTitles)) {
		printf("Error reading titles file %s.\n", pszFnTitles);
		exit(1);
	}

	// Open old .ini

	FILE *pfIniOld = fopen(pszFnIni, "r");
	if (pfIniOld == NULL) {
		printf("Error opening %s for reading.\n", pszFnIni);
		exit(1);
	}

	// Open new .ini

	FILE *pfIniNew = fopen(pszFnIniNew, "w");
	if (pfIniNew == NULL) {
		printf("Error opening %s for writing.\n", pszFnIniNew);
		exit(1);
	}

	// Start writing out new .ini with titles

	int nLevel = 0;
	IniSection *psecT = psec;
	IniSection *psecMax = &psec[cSections];
	while (psecT < psecMax) {
		char sz[1024];
		if (!ReadString(pfIniOld, psecT->nchSec, psecT->cchSec, (byte *)sz)) {
			printf("Error reading %s.\n", pszFnIni);
			exit(1);
		}

		bool fLevel = (strncmp(sz, "Level", 5) == 0);
		if (fLevel) {
			fprintf(pfIniNew, "; %d\n", nLevel++);
			fprintf(pfIniNew, "[Level]\n");
		} else {
			fprintf(pfIniNew, "[%s]\n", sz);
		}

		for (int n = 0; n < psecT->cprop; n++) {
			IniProperty *pprop = &psecT->pprop[n];
			if (!ReadString(pfIniOld, pprop->nchProp, pprop->cchProp, (byte *)sz)) {
				printf("Error reading %s.\n", pszFnIni);
				exit(1);
			}
			if (fLevel && strcmp(sz, "title") == 0)
				continue;
			fprintf(pfIniNew, "%s=", sz);
			if (!ReadString(pfIniOld, pprop->nchValue, pprop->cchValue, (byte *)sz)) {
				printf("Error reading %s.\n", pszFnIni);
				exit(1);
			}
			fprintf(pfIniNew, "%s\n", sz);
		}
		if (fLevel) {
			GetRandomTitle(sz, cchMax);
			fprintf(pfIniNew, "title=%s\n", sz);
		}
		fprintf(pfIniNew, "\n");
		psecT++;
	}
	fclose(pfIniNew);
	fclose(pfIniOld);

	// Free property sections

	int n;
	for (n = 0; n < cSections; n++)
		delete psec[n].pprop;
	delete psec;

	// Free titles

	for (n = 0; n < gcTitles; n++)
		delete gaszTitles[n];
	delete gaszTitles;

	return 0;
}
