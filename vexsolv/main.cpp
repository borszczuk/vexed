/*  main.c "solver / generator for Vexed puzzles"
    Copyright (C) 2001 Scott Ludwig (scottlu@eskimo.com)
    
    This file is part of Vexed.

    Vexed is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Vexed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vexed; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Solver / generator for vexed puzzles.

#ifdef _DEBUG
//#define INCL_DUMPLEVELS
//#define INCL_TRACE
//#define INCL_VALIDATE
#endif

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef __int64 HashKey;

#define knUnknown 0
#define knBeingSearched 1
#define knNoSolution 2
#define knHasSolution 3

void ExitUsage();

// Complete overkill
#define kcbHashTable (16 * 1024 * 1024)

struct HashEntry
{
	HashKey hk;
	HashEntry *pheNext;
#if 0
	int ply;
	int nState;
#endif
};

// Move

struct Move
{
	int x;
	int y;
	int nDir;
};

// MoveList

class MoveList
{
public:
	MoveList();
	~MoveList();
	bool InitFrom(MoveList *pml);
	bool AddMove(Move *pmv);
	void Print();
	int GetMoveCount();
	void GetMove(int n, Move *pmv);
	void SetMoveCount(int c);
	void SetMove(int n, Move *pmv);

	Move *m_pmv;
	int m_cMoves;
	int m_cMovesAlloc;
};

class Board
{
public:
	Board();
	~Board();
	void SetSize(int cx, int cy);
	bool Load(char *psz);
	int LoadRow(byte *pb, char *psz, char *pszMax);
	void Print();
	void PrintNotation();
	HashKey GetHashKey();
	void InitCounts();
	MoveList *GenerateMoves();
	void DoMove(Move *pmv);
	bool InitFrom(Board *pboard);
	bool DoGravityMoves();
	bool DoMatchMoves();
	int GetPieceCount();
	int GetSinglesCount();
	void SetPiece(int x, int y, byte bNew);
	void Search(MoveList *pmlPath, int ply, int plyMax, bool *pfStop);
	bool FindMatch();
	bool IsSolvable();
	void RandomGen(int cx, int cy, int cPiecesMin, int cPiecesMax, int cHoles);
	bool IsValidPlacement(int x, int y, byte bNew);
	bool HasSubspace();

	int GetStepCount();
	void GetStepMove(Move *pmv);
	int GetScore();
	int FindHoles();
	int ProfilePiece(byte bPc, int *acPcInSpace);
	void FillUnused(MoveList *pmlPath);
	int GetCrossoverCount();
	int CalcDifficultyScore(MoveList *pmlPath);

	HashKey m_hk;
	int m_cx;
	int m_cy;
	byte *m_pb;
	byte m_ab256[256];
	int m_cPieces;
	int m_cSingles;
	Board *m_pboardNext;
	int m_cCrossovers;
		
	Board *m_pboardLast;
	int m_cSteps;
	Move m_mvLast;
	byte *m_pbUsed;

	bool m_fFreeHoles;
	static byte *m_pbHoles;
};

byte *Board::m_pbHoles;
HashKey *gphk;
int gcHashEntries;
HashEntry **gpheHashTable;
int gcNodes;
dword gcmsSolve;

// Hashing

// Knuth vol 2, pp 26-27. This special random # generate ensures that the randomness is 32 bits worth rather than
// a number between 0 and n.

dword gadwInit[55] = {
    1410651636, 3012776752, 3497475623, 2892145026, 1571949714, 3253082284, 3489895018, 387949491, 2597396737, 1981903553,
    3160251843, 129444464, 1851443344, 4156445905, 224604922, 1455067070, 3953493484, 1460937157, 2528362617, 317430674, 
    3229354360, 117491133, 832845075, 1961600170, 1321557429, 747750121, 545747446, 810476036, 503334515, 4088144633,
    2824216555, 3738252341, 3493754131, 3672533954, 29494241, 1180928407, 4213624418, 33062851, 3221315737, 1145213552,
    2957984897, 4078668503, 2262661702, 65478801, 2527208841, 1960622036, 315685891, 1196037864, 804614524, 1421733266,
    2017105031, 3882325900, 810735053, 384606609, 2393861397
};
dword gadwRandom[55];
int gnRand1, gnRand2;

void RandomInit()
{
	memcpy(gadwRandom, gadwInit, sizeof(gadwInit));
	gnRand1 = 23;
	gnRand2 = 54;
}

dword RandomDword()
{
	dword dw = gadwRandom[gnRand2] += gadwRandom[gnRand1];
	gnRand1--;
	if (gnRand1 < 0)
		gnRand1 = 54;
	gnRand2--;
	if (gnRand2 < 0)
		gnRand2 = 54;
	return dw;
}

#define kcPieceTypes 256

void InitHashKeys(int cx, int cy)
{
	// Because of the way Vexed works, hash keys are needed for occupied squares
	// only. Don't need hash keys per piece per square, since per game only one
	// position can only be occupied in 1 way, which is independent of the piece
	// types. If the rules change, the hash keys will need to change.

	int cHashKeys = cx * cy;
	gphk = (HashKey *)new HashKey[cHashKeys][kcPieceTypes];

	RandomInit();
	HashKey *phk = gphk;
	for (int x = 0; x < cx; x++) {
		for (int y = 0; y < cy; y++) {
			for (int n = 0; n < kcPieceTypes; n++)
				*phk++ = (HashKey)((HashKey)RandomDword() | ((HashKey)RandomDword() << 32));
		}
	}
}

void ZeroHashTables()
{
	memset(gpheHashTable, 0, gcHashEntries * sizeof(HashEntry *));
}

void AllocHashTables(int cx, int cy)
{
	// Create the hash keys

	InitHashKeys(cx, cy);

	// Create the hash table.

	gcHashEntries = kcbHashTable / sizeof(HashEntry *);
	gpheHashTable = new HashEntry *[gcHashEntries];
	ZeroHashTables();
}

void FreeHashEntries()
{
	for (int n = 0; n < gcHashEntries; n++) {
		HashEntry *phe = gpheHashTable[n];
		while (phe != NULL) {
			HashEntry *pheNext = phe->pheNext;
			delete phe;
			phe = pheNext;
		}
		gpheHashTable[n] = NULL;
	}
}

void FreeHashTables()
{
	FreeHashEntries();
	delete gpheHashTable;
	delete gphk;
}

#if 0
HashEntry *GetHashTableEntry(HashKey hk)
{
	int n = ((dword)hk) % gcHashEntries;
	return &gpheHashTable[n];
}
#endif

bool IsHashed(HashKey hk)
{
	int n = ((dword)hk) % gcHashEntries;

	for (HashEntry *phe = gpheHashTable[n]; phe != NULL; phe = phe->pheNext) {
		if (phe->hk == hk)
			return true;
	}
	return false;
}

void AddToHash(HashKey hk)
{
	HashEntry *phe = new HashEntry;
	phe->hk = hk;

	int n = ((dword)hk) % gcHashEntries;
	if (gpheHashTable[n] == NULL) {
		gpheHashTable[n] = phe;
		phe->pheNext = NULL;
	} else {
		phe->pheNext = gpheHashTable[n];
		gpheHashTable[n] = phe;
	}
}

// MoveList

MoveList::MoveList()
{
	m_pmv = NULL;
	m_cMoves = 0;
	m_cMovesAlloc = 0;
}

MoveList::~MoveList()
{
	delete m_pmv;
}

#define kcmvAlloc 20
bool MoveList::AddMove(Move *pmv)
{
	if (m_cMoves == m_cMovesAlloc) {
		Move *pmvNew = new Move[m_cMovesAlloc + kcmvAlloc];
		if (pmvNew == NULL)
			return false;
		if (m_pmv != NULL) {
			memcpy(pmvNew, m_pmv, m_cMoves * sizeof(Move));
			delete m_pmv;
			m_pmv = pmvNew;
		} else {
			m_pmv = pmvNew;
		}
		m_cMovesAlloc += kcmvAlloc;
	}
	m_pmv[m_cMoves++] = *pmv;
	return true;
}

void MoveList::SetMoveCount(int c)
{
	delete m_pmv;
	m_pmv = new Move[c];
	m_cMoves = c;
	m_cMovesAlloc = c;
}

void MoveList::SetMove(int n, Move *pmv)
{
	if (n >= m_cMoves)
		_asm { int 3 };
	m_pmv[n] = *pmv;
}

bool MoveList::InitFrom(MoveList *pml)
{
	Move *pmvNew = new Move[pml->m_cMovesAlloc];
	if (pmvNew == NULL)
		return false;
	
	delete m_pmv;
	*this = *pml;

	m_pmv = pmvNew;
	memcpy(m_pmv, pml->m_pmv, m_cMoves * sizeof(Move));

	return true;
}

void MoveList::Print()
{
	if (m_cMoves == 0) {
		printf("no moves.\n");
		return;
	}

	for (int n = 0; n < m_cMoves; n++) {
		Move *pmv = &m_pmv[n];

      if (pmv->nDir < 0) {
		    printf("%c%c", pmv->x + 'A', pmv->y + 'a');
      } else {
		    printf("%c%c", pmv->x + 'a', pmv->y + 'A');
      }
	}
	printf("\n");
}

int MoveList::GetMoveCount()
{
	return m_cMoves;
}

void MoveList::GetMove(int n, Move *pmv)
{
	*pmv = m_pmv[n];
}

Board::Board()
{
	m_cCrossovers = 0;
	m_cx = -1;
	m_cy = -1;
	m_pb = NULL;
	m_hk = 0;
	m_cPieces = 0;
	m_cSingles = 0;
	m_pboardNext = NULL;
	memset(m_ab256, 0, sizeof(m_ab256));

	m_pboardLast = NULL;
	m_cSteps = 0;
	m_pbUsed = NULL;
	m_fFreeHoles = false;
}

Board::~Board()
{
	delete m_pb;
	delete m_pbUsed;
	if (m_fFreeHoles)
		delete m_pbHoles;
}

HashKey Board::GetHashKey()
{
	return m_hk;
}

int Board::GetStepCount()
{
	return m_cSteps;
}

int Board::GetCrossoverCount()
{
	return m_cCrossovers;
}

void Board::GetStepMove(Move *pmv)
{
	*pmv = m_mvLast;
}

int Board::GetScore()
{
	return m_cSteps + m_cPieces * 2;
}

void Board::InitCounts()
{
	m_hk = 0;
	m_cPieces = 0;
	m_cSingles = 0;
	memset(m_ab256, 0, sizeof(m_ab256));
	byte *pb = m_pb;
	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			if (*pb != 0 && *pb != 0xff) {
				byte b = *pb;
				*pb = 0;
				SetPiece(x, y, b);
			}
			pb++;
		}
	}
}

bool Board::InitFrom(Board *pboard)
{
	delete m_pb;
	*this = *pboard;
	m_fFreeHoles = false;
	m_pboardNext = pboard;
	m_pb = NULL;
	SetSize(m_cx, m_cy);
	memcpy(m_pb, pboard->m_pb, m_cx * m_cy);

	m_pboardLast = pboard;
	m_cSteps = pboard->m_cSteps;

	return true;
}

bool Board::FindMatch()
{
	Board *pboard = m_pboardNext;
	while (pboard != NULL) {
		if (pboard->m_hk == m_hk)
			return true;
		pboard = pboard->m_pboardNext;
	}
	return false;
}

bool Board::Load(char *psz)
{
	// Validate the board and get the dimensions

	char *pszLast = psz;
	bool fLoop = true;
	int cRows = 0;
	int cCols = -1;
	while (fLoop) {
		char *pszNext = strchr(pszLast, '/');
		if (pszNext == NULL) {
			pszNext = pszLast + strlen(pszLast);
			fLoop = false;
		}
		int cColsT = LoadRow(NULL, pszLast, pszNext);
		if (cCols == -1) {
			cCols = cColsT;
		} else if (cCols != cColsT) {
			printf("Error: row %d bad puzzle %s\n", cRows, psz);
			return false;
		}
		cRows++;
		pszLast = pszNext + 1;
	}

	// Set the size

	SetSize(cCols, cRows);

	// Load it this time

	byte *pbLoad = m_pb;
	pszLast = psz;
	fLoop = true;
	while (fLoop) {
		// Find the end of this row

		char *pszNext = strchr(pszLast, '/');
		if (pszNext == NULL) {
			pszNext = pszLast + strlen(pszLast);
			fLoop = false;
		}

		// Load the row

		LoadRow(pbLoad, pszLast, pszNext);

		// Next row...

		pbLoad += m_cx;
		pszLast = pszNext + 1;
	}

	// Analyze and find hole / bucket areas

	FindHoles();

	return true;
}

int Board::LoadRow(byte *pb, char *psz, char *pszMax)
{
	int cBlocks = 0;
	for (; psz < pszMax; psz++) {
		char ch = *psz;

		// Is this a number (non-moveable blocks?)
		
		int c = 0;
		while (ch >= '0' && ch <= '9') {
			c = c * 10 + ch - '0';
			psz++;
			if (psz >= pszMax)
				break;
			ch = *psz;
		}
		if (c != 0) {
			cBlocks += c;
			if (pb != NULL) {
				while (c != 0) {
					*pb++ = 0xff;
					c--;
				}
			}
			psz--;
			continue;
		}

		// Is this empty space?

		if (ch == '~') {
			cBlocks++;
			if (pb != NULL)
				*pb++ = 0;
			continue;
		}

		// This is a block type. Remember it verbatim

		cBlocks++;
		if (pb != NULL)
			*pb++ = ch;
	}

	return cBlocks;
}

int Board::FindHoles()
{
	int cb = m_cx * m_cy;
	m_pbHoles = new byte[cb];
	m_fFreeHoles = true;
	memset(m_pbHoles, 0xff, cb);

	// X         X        X
	// X   XXX   X    X   X
	// XXXXXXXXXXXXXXXXXXXX

	// - by scanline
	// - fHoles means no bottom holes
	// - find edges at runtime

	byte bHole = 0;
	int y;
	for (y = 0; y < m_cy; y++) {
		int xEdgeStart = 0;
		bool fHole = false;
		for (int x = 0; x <= m_cx; x++) {
			byte b = (x >= m_cx ? 0xff : m_pb[y * m_cx + x]);
			if (b != 0xff) {
				if (y != m_cy - 1 && m_pb[(y + 1) * m_cx + x] != 0xff)
					fHole = true;
			} else {
				if (!fHole) {
					bool fSet = false;
					for (int xT = xEdgeStart; xT < x; xT++) {
						fSet = true;
						m_pbHoles[y * m_cx + xT] = bHole;
					}
					if (fSet)
						bHole++;
				}
				xEdgeStart = x + 1;
				fHole = false;
			}
		}
	}

   return bHole;

#if 0
	printf("\n");
	for (y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			byte b = m_pbHoles[y * m_cx + x];
			printf("%c", b == 0xff ? 'X' : 'a' + b);
		}
		printf("\n");
	}
	printf("\n");
#endif
}

void Board::SetSize(int cx, int cy)
{
	m_cx = cx;
	m_cy = cy;
	delete m_pb;
	int cb = m_cx * m_cy;
	m_pb = new byte[cb];
	memset(m_pb, 0, cb);
}

void Board::Print()
{
	byte *pb = m_pb;
	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			byte b = *pb++;
			switch (b) {
			case 0:
				printf(" ");
				break;

			case 0xff:
				printf("*");
				break;

			default:
				printf("%c", b);
				break;
			}
		}
		printf("\n");
	}
}

void Board::PrintNotation()
{
	byte *pb = m_pb;
	for (int y = 0; y < m_cy; y++) {
		int c = 0;
		for (int x = 0; x < m_cx; x++) {
			byte b = *pb++;
			if (b == 0xff) {
				c++;
				if (x == m_cx - 1 || *pb != 0xff) {
					printf("%d", c);
					c = 0;
				}
				continue;
			}
			if (b == 0) {
				printf("~");
				continue;
			}
			printf("%c", b);
		}
		if (y != m_cy - 1)
			printf("/");
	}
}

MoveList *Board::GenerateMoves()
{
	MoveList *pml = new MoveList;
	if (pml == NULL)
		return NULL;

	byte *pb = m_pb;
	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			if (*pb == 0 || *pb == 0xff) {
				pb++;
				continue;
			}
			if (x > 0) {
				if (*(pb - 1) == 0) {
					Move mv;
					mv.x = x;
					mv.y = y;
					mv.nDir = -1;
					pml->AddMove(&mv);
				}
			}
			if (x < m_cx - 1) {
				if (*(pb + 1) == 0) {
					Move mv;
					mv.x = x;
					mv.y = y;
					mv.nDir = 1;
					pml->AddMove(&mv);
				}
			}
			pb++;
		}
	}

	return pml;
}

void Board::SetPiece(int x, int y, byte bNew)
{
	int n = y * m_cx + x;
	byte b = m_pb[n];
	if (b != 0 && b != 0xff) {
		m_ab256[b]--;
		switch (m_ab256[b]) {
		case 0:
			m_cSingles--;
			break;
		case 1:
			m_cSingles++;
			break;
		}
		m_cPieces--;
	}

	if (bNew != 0 && bNew != 0xff) {
		m_ab256[bNew]++;
		switch (m_ab256[bNew]){
		case 1:
			m_cSingles++;
			break;
		case 2:
			m_cSingles--;
			break;
		}
		m_cPieces++;
	}

	byte bOld = m_pb[n];
	if (bOld != 0)
		m_hk ^= gphk[n * kcPieceTypes + bOld];
	m_pb[n] = bNew;
	if (bNew != 0)
		m_hk ^= gphk[n * kcPieceTypes + bNew];

	if (m_pbUsed != NULL)
		m_pbUsed[n] = 1;
}

bool Board::DoGravityMoves()
{
	// Start at the bottom and go up since the blocks are moving down
	
	bool fChanged = false;
	byte *pb = &m_pb[m_cx * m_cy - 1];
	for (int y = m_cy - 1; y >= 0; y--) {
		for (int x = m_cx - 1; x >= 0; x--) {
			if (*pb == 0 || *pb == 0xff) {
				pb--;
				continue;
			}
			if (y >= m_cy - 1) {
				pb--;
				continue;
			}
			if (*(pb + m_cx) != 0) {
				pb--;
				continue;
			}

			// Make the move

			fChanged = true;
			SetPiece(x, y + 1, *pb);
			SetPiece(x, y, 0);
			pb--;
		}
	}
	return fChanged;
}

bool Board::DoMatchMoves()
{
	Board boardMatch;
	boardMatch.SetSize(m_cx, m_cy);

	byte *pbMatch = boardMatch.m_pb;
	byte *pb = m_pb;
	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			*pbMatch = 0;
			if (*pb != 0 && *pb != 0xff) {
				if (x > 0 && *(pb - 1) == *pb)
					*pbMatch |= 1;
				if (x < m_cx - 1 && *(pb + 1) == *pb)
					*pbMatch |= 1;
				if (y > 0 && *(pb - m_cx) == *pb)
					*pbMatch |= 1;
				if (y < m_cy - 1 && *(pb + m_cx) == *pb)
					*pbMatch |= 1;
			}
			pb++;
			pbMatch++;
		}
	}

	// Remove all matches

	bool fChanged = false;
	pb = m_pb;
	pbMatch = boardMatch.m_pb;
	for (y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			if (*pbMatch != 0) {
				// Make the change

				fChanged = true;
				SetPiece(x, y, 0);
			}
			pb++;
			pbMatch++;
		}
	}

	return fChanged;
}

int Board::GetPieceCount()
{
	return m_cPieces;
}

int Board::GetSinglesCount()
{
	return m_cSingles;
}

void Board::DoMove(Move *pmv)
{
	m_mvLast = *pmv;
	m_cSteps++;

	gcNodes++;

	int sq = pmv->y * m_cx + pmv->x;
	byte *pb = &m_pb[sq];
	byte bPc = *pb;
	SetPiece(pmv->x + pmv->nDir, pmv->y, *pb);
	SetPiece(pmv->x, pmv->y, 0);

	// Count crossover: when a move's destination is on top of another
	// piece or pulled out of the middle of a column


	if (pmv->y + 1 >= 0 && pmv->y + 1 < m_cy) {
		byte b = m_pb[(pmv->y + 1) * m_cx + pmv->x + pmv->nDir];
		if (b != 0 && b != 0xff && b != bPc) {
			//Print();
			m_cCrossovers++;
		}
		if (b == 0) {
			for (int y = pmv->y + 1; y < m_cy; y++) {
				b = m_pb[y * m_cx + pmv->x + pmv->nDir];
				if (b != 0 && b != 0xff && b != bPc) {
					//Print();
					m_cCrossovers++;
					break;
				}
			}
		}
	}
	if (pmv->y - 1 >= 0 && pmv->y - 1 < m_cy) {
		byte b = m_pb[(pmv->y - 1) * m_cx + pmv->x];
		if (b != 0 && b != 0xff) {
			//Print();
			m_cCrossovers++;
		}
	}

	// Do all gravity moves, then do match moves, and repeat until
	// there is no movement.

	do {
		while (DoGravityMoves())
			;
	} while (DoMatchMoves());

#ifdef INCL_VALIDATE
	Board boardT;
	boardT.InitFrom(this);
	boardT.InitCounts();
	if (boardT.m_hk != m_hk)
		_asm { int 3 };
	if (boardT.m_cPieces != m_cPieces)
		_asm { int 3 };
	if (boardT.m_cSingles != m_cSingles)
		_asm { int 3 };
#endif
}

void Board::Search(MoveList *pmlPath, int ply, int plyMax, bool *pfStop)
{
	// If the piece count is zero, we have a solution!

	if (GetPieceCount() == 0) {
		*pfStop = true;
		printf("%d Move Solution: ", pmlPath->GetMoveCount());
		pmlPath->Print();
		return;
	}

	if (ply >= plyMax)
		return;

	// Check the hash table. Has this position been seen before?

#if 0
	HashEntry *phe = GetHashTableEntry(m_hk);
	if (phe->hk != m_hk) {
		phe->hk = m_hk;
		phe->ply = ply;
		phe->nState = knUnknown;
	} else {
		// This position has been seen before. If it doesn't have
		// solutions don't search it further.

		if (ply >= phe->ply && phe->nState == knNoSolution) {
#ifdef INCL_TRACE
			printf("Rejected, node with no solution: node %d.\n", phe->cNodes);
#endif
			return false;
		}
	}
#endif

	// Can we tell if the position is not solvable?

	if (!IsSolvable())
		return;

	// Generate moves for this position
	
	MoveList *pml = GenerateMoves();

	// Enumerate through these moves

	int cNodesSav = gcNodes;
	for (int n = 0; n < pml->GetMoveCount(); n++) {
		// Clone the passed board. This way no need for UndoMove

		Board boardNew;
		boardNew.InitFrom(this);

		Move mv;
		pml->GetMove(n, &mv);
		boardNew.DoMove(&mv);

#ifdef INCL_TRACE
		printf("\n");
		printf("NODE: %d, CONTINUATION: %d\n", gcNodes, cNodesSav);
		Print();
		MoveList mlT;
		mlT.InitFrom(pmlPath);
		mlT.AddMove(&mv, 0);
		mlT.Print();
		boardNew.Print();
#endif

#if 0
		printf("Searching ply %d move %d / %d\n", ply, n, pml->GetMoveCount());
#endif

		if (FindMatch()) {
#ifdef INCL_TRACE
			printf("Repeated position.");
#endif
			delete pml;	
			return;
		}

		// Search this new position further

		MoveList mlNew;
		mlNew.InitFrom(pmlPath);
		mlNew.AddMove(&mv);

		boardNew.Search(&mlNew, ply + 1, plyMax, pfStop);
	}

	// Done with moves at this position; delete the move list

	delete pml;

#if 0
	if (phe != NULL && !fFoundSolution && !fUnknown)
		phe->nState = knNoSolution;
#endif
}

void RandInit()
{
	srand(GetTickCount());
}

int RandInt(int a, int b)
{
	return (rand() % ((b - a) + 1)) + a;
}

bool Board::HasSubspace()
{
	// Flood fill the first empty space, then check for other empty spaces.
	// This tells us if we just placed a brick which closed off a space, creating
	// a sub space.

	// Mark first empty space

	Board board;
	board.InitFrom(this);
	int cb = board.m_cx * board.m_cy;
	for (int n = 0; n < cb; n++) {
		if (board.m_pb[n] == 0) {
			board.m_pb[n] = 0xfe;
			break;
		}
	}

	// Flood fill space

	bool fNoChange = false;
	while (!fNoChange) {
		fNoChange = true;
		for (int x = 1; x < board.m_cx - 1; x++) {
			for (int y = 1; y < board.m_cy - 1; y++) {
				if (board.m_pb[y * board.m_cx + x] != 0xfe)
					continue;
				if (board.m_pb[(y + 1) * board.m_cx + x] == 0) {
					board.m_pb[(y + 1) * board.m_cx + x] = 0xfe;
					fNoChange = false;
				}
				if (board.m_pb[(y - 1) * board.m_cx + x] == 0) {
					board.m_pb[(y - 1) * board.m_cx + x] = 0xfe;
					fNoChange = false;
				}
				if (board.m_pb[y * board.m_cx + x + 1] == 0) {
					board.m_pb[y * board.m_cx + x + 1] = 0xfe;
					fNoChange = false;
				}
				if (board.m_pb[y * board.m_cx + x - 1] == 0) {
					board.m_pb[y * board.m_cx + x - 1] = 0xfe;
					fNoChange = false;
				}
			}
		}
	}

	// See if there is any empty space which will mean the
	// space is subspace.

	for (n = 0; n < cb; n++) {
		if (board.m_pb[n] == 0)
			return true;
	}

	// Not partitioned

	return false;
}

bool Board::IsValidPlacement(int x, int y, byte bNew)
{
	if (m_pb[y * m_cx + x] != 0)
		return false;
	if (m_pb[(y + 1) * m_cx + x] == 0)
		return false;
	if (m_pb[(y + 1) * m_cx + x] == bNew)
		return false;
	if (m_pb[y * m_cx + x + 1] == bNew)
		return false;
	if (m_pb[y * m_cx + x - 1] == bNew)
		return false;

	return true;
}

void Board::RandomGen(int cx, int cy, int cPiecesMin, int cPiecesMax, int cHoles)
{
	SetSize(cx, cy);

	// Create a board with bricks (without pieces) that matches our criteria

	while (true) {
		// Set bricks around sides

		int x, y;
		memset(m_pb, 0, m_cx * m_cy);
		memset(m_pb, 0xff, m_cx);
		memset(&m_pb[(cy - 1) * m_cx], 0xff, m_cx);
		for (y = 0; y < m_cy; y++) {
			m_pb[y * m_cx] = 0xff;
			m_pb[y * m_cx + m_cx - 1] = 0xff;
		}

		// Random brick placement

		int cBricks = RandInt(4, 10);
		while (cBricks > 0) {
			x = RandInt(1, m_cx - 2);
			y = RandInt(1, m_cy - 2);

			// Did this create a subspace?

			m_pb[y * m_cx + x] = 0xff;
			if (HasSubspace()) {
				m_pb[y * m_cx + x] = 0;
				continue;
			}

			cBricks--;
		}

		// Check finished board for hole limit

		if (cHoles != -1) {
			if (FindHoles() < cHoles)
				continue;
		}

		break;
	}

	// Place pieces

	if (cPiecesMin == -1)
		cPiecesMin = 6;
	if (cPiecesMax == -1)
		cPiecesMax = 32;

	while (true) {
		int cPiecesPlace = RandInt(cPiecesMin, cPiecesMax);

		int n;
		for (n = 0; n < cPiecesPlace; n++) {
			int x, y;
			byte bNew;
			do {
				x = RandInt(1, m_cx - 2);
				y = RandInt(1, m_cy - 2);
				bNew = RandInt('a', 'h');
			} while (!IsValidPlacement(x, y, bNew));

			SetPiece(x, y, bNew);
		}
			
		// We may have a few pieces with piece counts of 1. Add pieces as necessary

		for (n = 0; n < 256; n++) {
			// This piece only have piece count of 1?

			if (m_ab256[n] != 1)
				continue;

			// Place this piece

			int x, y;
			byte bNew = (byte)n;
			do {
				x = RandInt(1, m_cx - 2);
				y = RandInt(1, m_cy - 2);
			} while (!IsValidPlacement(x, y, bNew));

			SetPiece(x, y, bNew);
		}

#if 0
		FindHoles();
		return;

#else
		// Do we have allowable piece counts?

		if (m_cPieces >= cPiecesMin && m_cPieces <= cPiecesMax) {
			FindHoles();
			return;
		}

		// Erase and do again

		for (int x = 0; x < m_cx; x++) {
			for (int y = 0; y < m_cy; y++) {
				byte b = m_pb[y * m_cx + x];
				if (b != 0 && b != 0xff)
					SetPiece(x, y, 0);
			}
		}
#endif
	}
}

bool Board::IsSolvable()
{
	// If there is 1 type of a piece, no solution
	
	if (GetSinglesCount() != 0)
		return false;

	// We must try to figure out as early as possible if a position is
	// solvable so that the search tree gets pruned to a reasonable size.
	//
	// If we have the following patterns against a wall, the position
	// is not solvable:
	//
	// (0 B) - illegal single
	// ABA
	//
	// (<= 1 B)
	// ABAB
	//
	// (<= 1 A)
	// AXA
	//
	// (0 A)
	// A A A
	//
	// XBAX
	// XABX
	// XXXX

	// See if we have any piece types split between N buckets (case 2)

	byte acPcVisited[256];
	int acPcInSpace[32];
	
	memset(acPcVisited, 0, sizeof(acPcVisited));

	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			byte bHole = m_pbHoles[y * m_cx + x];
			if (bHole == 0xff)
				continue;
			byte bPc = m_pb[y * m_cx + x];
			if (bPc == 0)
				continue;
			if (acPcVisited[bPc]++ != 0)
				continue;
			memset(acPcInSpace, 0, sizeof(acPcInSpace));
			int cOutside = ProfilePiece(bPc, acPcInSpace);

			// Find the number of spaces with single pieces. If the number
			// of pieces outside spaces is less than this count, then the
			// puzzle is not solvable.

			int n;
			int cSingles = 0;
			for (n = 0; n < 32; n++) {
				if (acPcInSpace[n] == 1)
					cSingles++;
			}
			if (cOutside < cSingles)
				return false;

			// Check for the ABAB case and A A A case

			for (n = 0; n < 32; n++) {
				// All pieces of this type in this space?

				if (acPcInSpace[n] == m_ab256[bPc]) {
					// Odd number? Can't solve 

					if (acPcInSpace[n] & 1)
						return false;

					// Check for two separated pieces

					if (acPcInSpace[n] != 2)
						continue;

					byte ab256Between[256];
					memset(ab256Between, 0, sizeof(ab256Between));
					for (int xT = x + 1; xT < m_cx; xT++) {
						byte bPcT = m_pb[y * m_cx + xT];

						// Found same piece? done.
						if (bPcT == bPc)
							break;

						// Different piece. If not all in this space,
						// continue.

						int acPcInSpaceT[32];
						memset(acPcInSpaceT, 0, sizeof(acPcInSpaceT));
						ProfilePiece(bPcT, acPcInSpaceT);
						if (acPcInSpaceT[n] != m_ab256[bPcT])
							continue;
						ab256Between[bPcT]++;
					}

					// Odd # of pieces between? If so, the level
					// can't be solved.

					for (int i = 0; i < 256; i++) {
						if (ab256Between[i] & 1)
							return false;
					}
				}
			}
		}
	}

	return true;
}

int Board::ProfilePiece(byte bPc, int *acPcInSpace)
{
	int cOutside = 0;
	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			byte bPcT = m_pb[y * m_cx + x];
			if (bPcT != bPc)
				continue;
			byte bHole = m_pbHoles[y * m_cx + x];
			if (bHole == 0xff) {
				cOutside++;
			} else {
				acPcInSpace[bHole]++;
			}
		}
	}
	return cOutside;
}

void Board::FillUnused(MoveList *pmlPath)
{
	Board boardNew;
	boardNew.InitFrom(this);
	byte *pbUsed = new byte[m_cx * m_cy];
	memset(pbUsed, 0, m_cx * m_cy);
	boardNew.m_pbUsed = pbUsed;

	for (int n = 0; n < pmlPath->GetMoveCount(); n++) {
		Move mv;
		pmlPath->GetMove(n, &mv);
		boardNew.DoMove(&mv);
	}
	if (boardNew.GetPieceCount() != 0)
		_asm { int 3 };

	for (int y = 0; y < m_cy; y++) {
		for (int x = 0; x < m_cx; x++) {
			if (pbUsed[y * m_cx + x] == 0)
				m_pb[y * m_cx + x] = 0xff;
		}
	}
	boardNew.m_pbUsed = NULL;
	delete pbUsed;
}

// With solution in hand, calc a difficulty score

int Board::CalcDifficultyScore(MoveList *pmlPath)
{
	Board boardT;
	boardT.InitFrom(this);
	int cMoves = pmlPath->GetMoveCount();
	int n;
	for (n = 0; n < cMoves; n++) {
		Move mv;
		pmlPath->GetMove(n, &mv);
		boardT.DoMove(&mv);
	}
	int cCrossovers = boardT.m_cCrossovers;

	// Scoring elements:
	// - crossover count (# times a piece is moved over another piece or
	//   pulled from a stack)
	// - total piece count. Generally gets harder with more pieces
	// - Hole count (how many buckets actually. Buckets force planning)
	// - number of piece types that have odd totals (forcing a 3 piece
	//   match at some point)

	// - crossover - non-linear scoring, evidence of planning
	// - piece count - non-linear scaling factor
	// - holes >= 2 - major planning element
	// - 3 piece matches - minor planning element

	int nScore = 0;

	// 20 for each hole

	int cHoles = FindHoles();
	if (cHoles >= 2) {
		int cT = cCrossovers - cHoles;
		if (cT > 0)
			nScore += cT * 10;
	} else {
		// Non-linear advance for each crossover

		nScore += cCrossovers * cCrossovers / 2;
	}

	// 5 for each 3 piece match

	int cThrees = 0;
	for (n = 0; n < 256; n++) {
		if (m_ab256[n] & 1)
			cThrees++;
	}
	nScore += cThrees * 5;

	// Non-linear scaling for each piece
	
	float flScale = 1.0f + (m_cPieces <= 8 ? 0.0f : (float)(m_cPieces - 8) * .05f);
	nScore = (int)((float)(nScore + m_cPieces) * flScale);

	return nScore;
}

#ifdef INCL_DUMPLEVELS
// This data is from vexed

#if 0
// v1.0
int level [59] [8] [10] = 
{
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 2, 1, 0, 0, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 0, 0, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 0, 0, 9, 9, 9 },
		{ 9, 9, 9, 1, 0, 0, 2, 9, 9, 9 },
		{ 9, 9, 9, 9, 1, 2, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 1, 0, 0, 9, 9 },
		{ 9, 9, 9, 0, 0, 2, 0, 0, 9, 9 },
		{ 9, 9, 9, 0, 1, 3, 0, 0, 9, 9 },
		{ 9, 9, 9, 0, 2, 4, 0, 0, 9, 9 },
		{ 9, 9, 9, 2, 4, 3, 0, 3, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 9, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 0, 0, 9, 9, 9 },
		{ 9, 9, 5, 0, 7, 6, 0, 5, 9, 9 },
		{ 9, 9, 6, 0, 9, 9, 0, 7, 9, 9 },
		{ 9, 9, 9, 6, 0, 0, 7, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 2, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 1, 3, 0, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 1, 0, 1, 0, 9, 9 },
		{ 9, 9, 9, 9, 3, 0, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 2, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 1, 0, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 4, 0, 4, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 0, 5, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 4, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 5, 9, 9, 9 },
		{ 9, 9, 9, 9, 3, 9, 1, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 0, 0, 0, 9, 9 },
		{ 9, 9, 9, 5, 0, 0, 0, 7, 9, 9 },
		{ 9, 9, 9, 9, 6, 0, 0, 9, 9, 9 },
		{ 9, 9, 9, 6, 7, 0, 2, 7, 9, 9 },
		{ 9, 9, 9, 5, 6, 0, 7, 2, 9, 9 },
		{ 9, 9, 9, 9, 5, 7, 2, 7, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 1, 3, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 3, 4, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 4, 1, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 1, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 0, 0, 0, 0, 0, 4, 9, 9 },
		{ 9, 9, 0, 0, 0, 0, 0, 5, 9, 9 },
		{ 9, 9, 2, 0, 0, 1, 0, 2, 9, 9 },
		{ 9, 9, 1, 5, 0, 5, 0, 9, 9, 9 },
		{ 9, 9, 5, 2, 4, 9, 2, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 0, 2, 9, 9, 9 },
		{ 9, 9, 9, 1, 0, 0, 3, 2, 9, 9 },
		{ 9, 9, 9, 9, 1, 0, 2, 3, 9, 9 },
		{ 9, 9, 9, 9, 9, 0, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 0, 0, 0, 0, 0, 0, 9, 9 },
                	{ 9, 3, 0, 0, 0, 0, 3, 0, 0, 9 },
           	     	{ 9, 4, 5, 4, 0, 0, 4, 7, 0, 9 },
                	{ 9, 5, 2, 3, 0, 0, 7, 3, 2, 9 },
                	{ 9, 7, 5, 4, 0, 9, 2, 5, 6, 9 },
                	{ 9, 2, 7, 5, 9, 9, 5, 6, 4, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 0, 0, 0, 0, 3, 4, 9, 9 },
                	{ 9, 9, 0, 0, 5, 0, 9, 9, 9, 9 },
                	{ 9, 9, 0, 0, 9, 0, 0, 0, 9, 9 },
               	{ 9, 9, 4, 0, 9, 0, 0, 0, 9, 9 },
                	{ 9, 9, 9, 9, 0, 0, 7, 3, 9, 9 },
                	{ 9, 9, 3, 0, 0, 5, 3, 7, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 2, 1, 3, 0, 3, 1, 2, 9 },
                	{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9 },
                	{ 9, 9, 9, 1, 0, 0, 0, 1, 9, 9 },
                	{ 9, 9, 9, 9, 0, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 0, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 2, 1, 3, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 1, 3, 4, 3, 0, 4, 3, 1, 9 },
                	{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 4, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 5, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 7, 0, 0, 9, 9, 9 },
                	{ 9, 9, 9, 0, 6, 0, 6, 5, 9, 9 },
                	{ 9, 9, 9, 9, 7, 9, 5, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 1, 0, 9, 9, 9 },
                	{ 9, 9, 9, 1, 0, 3, 0, 1, 9, 9 },
                	{ 9, 9, 9, 9, 0, 1, 0, 3, 9, 9 },
                	{ 9, 9, 9, 3, 0, 9, 3, 1, 9, 9 },
                	{ 9, 9, 9, 9, 3, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 2, 1, 2, 0, 3, 2, 4, 9 },
                	{ 9, 9, 9, 2, 4, 0, 4, 1, 9, 9 },
                	{ 9, 9, 9, 9, 3, 0, 1, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 4, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 1, 3, 4, 0, 1, 9, 9 },
                	{ 9, 9, 9, 5, 9, 5, 1, 4, 9, 9 },
                	{ 9, 9, 9, 7, 0, 7, 5, 3, 9, 9 },
                	{ 9, 9, 9, 9, 0, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 5, 7, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 7, 1, 9, 9, 9, 9, 9 },
	    	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 5, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 7, 0, 0, 9, 9 },
                	{ 9, 9, 9, 6, 0, 6, 0, 9, 9, 9 },
                	{ 9, 9, 9, 7, 0, 5, 0, 9, 9, 9 },
                	{ 9, 9, 9, 9, 0, 7, 0, 0, 9, 9 },
                	{ 9, 9, 9, 9, 9, 5, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 2, 0, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 2, 9, 9, 9 },
                	{ 9, 9, 1, 3, 0, 0, 4, 9, 9, 9 },
                	{ 9, 9, 4, 1, 0, 0, 2, 3, 9, 9 },
                	{ 9, 9, 3, 4, 0, 4, 3, 9, 9, 9 },
                	{ 9, 9, 9, 9, 0, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 0, 1, 9, 9, 9 },
                	{ 9, 9, 9, 9, 0, 0, 3, 0, 9, 9 },
                	{ 9, 9, 9, 4, 0, 0, 5, 0, 9, 9 },
                	{ 9, 9, 9, 9, 1, 3, 9, 5, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 4, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
        	{
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 0, 2, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 1, 3, 9, 9, 9 },
                	{ 9, 9, 9, 3, 0, 2, 1, 9, 9, 9 },
                	{ 9, 9, 9, 4, 0, 3, 4, 0, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
                	{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
        	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 6, 0, 0, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 9, 9, 9 },
        		{ 9, 9, 8, 0, 0, 0, 6, 8, 9, 9 },
        		{ 9, 9, 9, 9, 0, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 8, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 6, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 2, 9 },
        		{ 9, 9, 0, 0, 0, 0, 1, 0, 3, 9 },
        		{ 9, 9, 4, 0, 5, 0, 7, 0, 1, 9 },
        		{ 9, 9, 3, 0, 2, 0, 6, 0, 6, 9 },
        		{ 9, 9, 4, 0, 7, 0, 5, 0, 5, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 4, 0, 4, 0, 0, 9, 9 },
        		{ 9, 9, 9, 9, 0, 5, 0, 5, 9, 9 },
        		{ 9, 9, 9, 5, 0, 7, 0, 7, 9, 9 },
        		{ 9, 9, 9, 4, 0, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 0, 2, 1, 3, 4, 0, 9, 9 },
        		{ 9, 9, 0, 9, 9, 9, 5, 0, 9, 9 },
        		{ 9, 9, 5, 0, 9, 0, 2, 0, 9, 9 },
        		{ 9, 9, 4, 0, 0, 0, 9, 5, 9, 9 },
        		{ 9, 9, 3, 1, 9, 5, 2, 4, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 2, 1, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 3, 2, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 1, 3, 0, 4, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 3, 9, 9, 9 },
        		{ 9, 9, 9, 3, 1, 0, 2, 9, 9, 9 },
        		{ 9, 9, 9, 4, 9, 0, 1, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 3, 9, 9, 9 },
        		{ 9, 9, 9, 9, 0, 0, 4, 3, 9, 9 },
        		{ 9, 9, 9, 4, 0, 5, 7, 9, 9, 9 },
        		{ 9, 9, 9, 9, 3, 7, 3, 0, 9, 9 },
        		{ 9, 9, 9, 9, 9, 5, 4, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 4, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 5, 9, 9, 9, 9 },
        		{ 9, 9, 9, 7, 0, 7, 0, 0, 9, 9 },
        		{ 9, 9, 9, 4, 7, 9, 6, 4, 9, 9 },
        		{ 9, 9, 4, 5, 9, 9, 9, 6, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 2, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 1, 9, 9 },
        		{ 9, 9, 9, 1, 0, 0, 0, 3, 9, 9 },
        		{ 9, 9, 9, 3, 0, 0, 1, 2, 9, 9 },
        		{ 9, 9, 9, 4, 0, 4, 3, 9, 9, 9 },
        		{ 9, 9, 9, 3, 0, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 1, 9, 0, 0, 3, 0, 9, 9 },
        		{ 9, 9, 3, 9, 0, 0, 4, 0, 9, 9 },
        		{ 9, 9, 5, 0, 7, 0, 5, 4, 9, 9 },
        		{ 9, 9, 9, 9, 3, 0, 9, 3, 9, 9 },
        		{ 9, 9, 9, 9, 9, 1, 9, 7, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 2, 0, 1, 0, 9, 9 },
        		{ 9, 9, 9, 0, 1, 2, 3, 0, 9, 9 },
        		{ 9, 9, 9, 4, 3, 1, 2, 0, 9, 9 },
        		{ 9, 9, 9, 2, 5, 3, 1, 5, 9, 9 },
        		{ 9, 9, 9, 9, 4, 5, 2, 1, 9, 9 },
        		{ 9, 9, 9, 9, 9, 4, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 0, 0, 9, 9, 9, 9 },
        		{ 9, 2, 0, 0, 1, 3, 0, 0, 9, 9 },
        		{ 9, 9, 0, 0, 3, 9, 0, 0, 0, 9 },
        		{ 9, 4, 0, 0, 4, 0, 0, 0, 2, 9 },
        		{ 9, 9, 1, 0, 9, 0, 0, 9, 9, 9 },
        		{ 9, 9, 4, 9, 9, 9, 3, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 2, 1, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 3, 4, 5, 9, 9, 9 },
        		{ 9, 9, 0, 0, 7, 1, 6, 5, 9, 9 },
        		{ 9, 9, 0, 0, 3, 4, 8, 7, 9, 9 },
        		{ 9, 9, 9, 0, 8, 6, 7, 9, 9, 9 },
        		{ 9, 9, 9, 9, 5, 2, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 3, 0, 0, 0, 0, 4, 9, 9 },
        		{ 9, 9, 9, 9, 0, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 3, 5, 3, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 4, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 5, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 4, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 2, 1, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 2, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 0, 9, 9, 9 },
        		{ 9, 9, 9, 4, 1, 3, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 4, 0, 9, 9, 9 },
        		{ 9, 9, 9, 2, 1, 3, 2, 4, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 1, 3, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 4, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 5, 0, 9, 9, 9, 9 },
        		{ 9, 9, 0, 0, 3, 0, 0, 5, 9, 9 },
        		{ 9, 9, 3, 0, 9, 0, 0, 4, 9, 9 },
        		{ 9, 9, 1, 3, 1, 4, 5, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 0, 2, 0, 0, 0, 0, 9, 9 },
        		{ 9, 9, 0, 1, 0, 0, 3, 0, 9, 9 },
        		{ 9, 9, 1, 9, 1, 0, 9, 3, 9, 9 },
        		{ 9, 9, 9, 0, 4, 5, 0, 9, 9, 9 },
        		{ 9, 9, 5, 0, 3, 4, 5, 2, 9, 9 },
        		{ 9, 9, 9, 1, 5, 3, 2, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 2, 1, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 1, 2, 9 },
        		{ 9, 9, 9, 0, 0, 0, 2, 3, 1, 9 },
        		{ 9, 9, 4, 1, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 3, 2, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 4, 3, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 2, 1, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 3, 4, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 5, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 5, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 0, 1, 4, 9, 9, 9 },
        		{ 9, 9, 9, 0, 0, 3, 2, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 2, 1, 9 },
        		{ 9, 9, 9, 9, 9, 0, 0, 3, 2, 9 },
        		{ 9, 9, 9, 9, 9, 0, 2, 4, 1, 9 },
        		{ 9, 9, 3, 1, 0, 0, 9, 9, 9, 9 },
        		{ 9, 9, 4, 2, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 1, 4, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 0, 2, 1, 9, 9, 9 },
        		{ 9, 9, 9, 3, 0, 1, 9, 9, 9, 9 },
        		{ 9, 9, 4, 1, 0, 3, 0, 1, 9, 9 },
        		{ 9, 9, 1, 2, 0, 4, 1, 9, 9, 9 },
        		{ 9, 9, 4, 1, 0, 1, 4, 9, 9, 9 },
        		{ 9, 9, 9, 9, 1, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 0, 0, 0, 9, 9, 9, 9 },
        		{ 9, 9, 9, 3, 4, 0, 0, 9, 9, 9 },
        		{ 9, 9, 9, 9, 5, 4, 0, 0, 9, 9 },
        		{ 9, 9, 9, 9, 9, 3, 0, 5, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 0, 3, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 1, 0, 0, 9, 9, 9, 9, 9 },
		{ 9, 9, 3, 0, 9, 3, 9, 9, 9, 9 },
		{ 9, 9, 9, 4, 0, 4, 5, 9, 9, 9 },
		{ 9, 9, 9, 5, 0, 1, 4, 5, 9, 9 },
		{ 9, 9, 9, 9, 0, 4, 1, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 1, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 2, 1, 0, 0, 0, 3, 9, 9 },
		{ 9, 9, 4, 3, 4, 0, 0, 4, 9, 9 },
		{ 9, 9, 9, 2, 3, 1, 0, 3, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 0, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 0, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 2, 9, 0, 0, 1, 9, 9 },
		{ 9, 9, 9, 1, 9, 0, 2, 3, 9, 9 },
		{ 9, 9, 9, 4, 0, 3, 5, 2, 9, 9 },
		{ 9, 9, 9, 9, 0, 4, 2, 5, 9, 9 },
		{ 9, 9, 9, 9, 0, 2, 5, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 1, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 2, 0, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 1, 0, 0, 0, 0, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 0, 2, 9, 9 },
		{ 9, 9, 9, 0, 0, 1, 0, 1, 9, 9 },
		{ 9, 9, 9, 0, 0, 2, 9, 2, 9, 9 },
		{ 9, 9, 9, 0, 0, 1, 2, 1, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 0, 2, 1, 0, 3, 1, 4, 9 },
		{ 9, 9, 0, 3, 5, 0, 5, 4, 9, 9 },
		{ 9, 9, 0, 9, 9, 0, 9, 9, 9, 9 },
		{ 9, 9, 3, 5, 2, 0, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 0, 2, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 2, 0, 0, 9, 9 },
		{ 9, 9, 9, 1, 0, 3, 0, 0, 9, 9 },
		{ 9, 9, 9, 9, 0, 9, 0, 0, 9, 9 },
		{ 9, 9, 9, 0, 0, 3, 0, 0, 9, 9 },
		{ 9, 9, 9, 1, 0, 1, 0, 3, 9, 9 },
		{ 9, 9, 9, 2, 0, 2, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 2, 1, 0, 9, 9, 9, 9, 9 },
		{ 9, 9, 3, 2, 4, 0, 0, 9, 9, 9 },
		{ 9, 3, 9, 9, 9, 9, 0, 0, 9, 9 },
		{ 9, 5, 0, 0, 0, 0, 5, 0, 0, 9 },
		{ 9, 3, 0, 4, 2, 4, 2, 1, 2, 9 },
		{ 9, 9, 4, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 2, 0, 1, 9, 9, 9, 9 },
		{ 9, 0, 0, 1, 0, 9, 0, 0, 3, 9 },
		{ 9, 2, 0, 4, 0, 0, 3, 4, 9, 9 },
		{ 9, 9, 9, 9, 0, 0, 5, 9, 9, 9 },
		{ 9, 9, 9, 4, 0, 5, 9, 9, 9, 9 },
		{ 9, 9, 9, 5, 0, 1, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 2, 0, 0, 0, 0, 1, 9, 9 },
		{ 9, 3, 4, 0, 2, 1, 0, 5, 3, 9 },
		{ 9, 7, 5, 0, 9, 9, 0, 3, 4, 9 },
		{ 9, 9, 9, 0, 0, 0, 0, 9, 9, 9 },
		{ 9, 9, 9, 0, 0, 0, 7, 9, 9, 9 },
		{ 9, 9, 9, 9, 7, 5, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 2, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 1, 9, 9, 9, 9 },
		{ 9, 0, 0, 0, 0, 2, 0, 3, 4, 9 },
		{ 9, 5, 0, 0, 0, 1, 0, 4, 5, 9 },
		{ 9, 9, 9, 9, 0, 2, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 0, 3, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 2, 1, 3, 9, 9, 9 },
		{ 9, 9, 9, 9, 4, 2, 1, 9, 9, 9 },
		{ 9, 9, 9, 0, 3, 9, 4, 0, 9, 9 },
		{ 9, 9, 0, 0, 9, 9, 9, 0, 1, 9 },
		{ 9, 9, 0, 0, 4, 2, 4, 0, 3, 9 },
		{ 9, 9, 4, 3, 9, 9, 9, 3, 2, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 0, 0, 2, 1, 3, 0, 9, 9 },
		{ 9, 9, 0, 9, 9, 9, 9, 0, 1, 9 },
		{ 9, 9, 0, 0, 0, 4, 0, 0, 5, 9 },
		{ 9, 9, 5, 0, 0, 5, 0, 0, 2, 9 },
		{ 9, 9, 3, 0, 2, 3, 0, 0, 7, 9 },
		{ 9, 9, 2, 3, 5, 9, 1, 7, 4, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 2, 1, 0, 0, 0, 0, 3, 4, 9 },
		{ 9, 4, 5, 3, 0, 0, 7, 1, 5, 9 },
		{ 9, 5, 2, 4, 0, 0, 5, 4, 7, 9 },
		{ 9, 7, 4, 2, 0, 0, 9, 7, 4, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 0, 2, 9, 1, 0, 0, 9, 9 },
		{ 9, 9, 3, 4, 9, 9, 9, 0, 9, 9 },
		{ 9, 9, 4, 5, 0, 0, 9, 0, 9, 9 },
		{ 9, 9, 3, 7, 9, 0, 9, 0, 9, 9 },
		{ 9, 9, 7, 5, 2, 0, 0, 1, 9, 9 },
		{ 9, 9, 6, 2, 3, 0, 3, 6, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 0, 0, 0, 0, 0, 2, 0, 0, 9 },
		{ 9, 1, 0, 0, 3, 0, 4, 0, 0, 9 },
		{ 9, 5, 0, 0, 4, 3, 1, 2, 0, 9 },
		{ 9, 7, 0, 0, 3, 4, 2, 6, 0, 9 },
		{ 9, 5, 0, 0, 7, 8, 6, 2, 8, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 9, 9, 9, 2, 0, 9, 9, 0, 0, 9 },
                	{ 9, 9, 9, 9, 0, 0, 7, 0, 1, 9 },
		{ 9, 9, 9, 0, 7, 0, 1, 0, 9, 9 },
                	{ 9, 9, 0, 0, 5, 1, 2, 0, 9, 9 },
		{ 9, 9, 9, 0, 2, 9, 7, 0, 9, 9 },
		{ 9, 9, 9, 5, 7, 9, 1, 2, 1, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	},
	{
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
		{ 2, 0, 9, 9, 9, 9, 1, 0, 1, 9 },
		{ 9, 0, 9, 2, 0, 0, 3, 0, 4, 9 },
		{ 9, 0, 9, 9, 0, 0, 5, 0, 5, 9 },
		{ 9, 2, 0, 5, 4, 0, 3, 0, 3, 7 },
		{ 9, 9, 9, 9, 5, 0, 9, 0, 7, 5 },
		{ 9, 9, 9, 9, 3, 0, 9, 0, 9, 9 },
		{ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
	}
};
#else
// v1.4
int level [59] [9] [11] = 
{
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 8, 6, 0, 0, 0, 5, 0, 9},
      {9, 0, 5, 1, 2, 0, 0, 6, 8, 9},
      {9, 0, 9, 9, 9, 0, 0, 9, 9, 9},
      {9, 0, 9, 3, 0, 0, 0, 0, 9, 9},
      {9, 0, 0, 2, 0, 1, 0, 3, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },      
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 9, 0, 9, 9, 0, 9},
      {9, 0, 0, 4, 0, 0, 0, 0, 3, 9},
      {9, 0, 0, 9, 0, 0, 0, 0, 9, 9},
      {9, 0, 4, 9, 0, 0, 0, 0, 0, 9},
      {9, 8, 1, 0, 0, 0, 3, 0, 6, 9},
      {9, 2, 6, 0, 2, 0, 9, 8, 1, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 1, 0, 9, 0, 0, 9, 9},
      {9, 0, 0, 6, 0, 0, 0, 0, 0, 9},
      {9, 7, 0, 9, 0, 0, 3, 0, 0, 9},
      {9, 5, 2, 0, 0, 8, 4, 0, 4, 9},
      {9, 3, 1, 5, 0, 5, 2, 4, 8, 9},
      {9, 5, 6, 1, 6, 4, 7, 9, 6, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 8, 0, 9, 9, 1, 4, 9, 0, 9},
      {9, 9, 0, 0, 9, 9, 6, 4, 0, 9},
      {9, 3, 0, 0, 0, 0, 9, 9, 0, 9},
      {9, 9, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 3, 0, 1, 2, 0, 0, 2, 9},
      {9, 9, 1, 0, 8, 6, 0, 0, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 9, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 9, 9, 0, 5, 0, 0, 9},
      {9, 0, 5, 0, 0, 9, 9, 0, 0, 9},
      {9, 0, 1, 0, 0, 0, 0, 3, 6, 9},
      {9, 0, 9, 2, 8, 6, 0, 8, 9, 9},
      {9, 0, 1, 9, 9, 8, 9, 3, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 8, 9, 9, 0, 9, 9, 4, 0, 9},
      {9, 5, 0, 0, 0, 9, 0, 7, 0, 9},
      {9, 9, 0, 0, 0, 0, 0, 9, 5, 9},
      {9, 0, 0, 1, 0, 0, 6, 7, 2, 9},
      {9, 0, 0, 9, 0, 0, 4, 6, 1, 9},
      {9, 9, 0, 8, 0, 6, 9, 2, 6, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 8, 0, 0, 0, 2, 0, 1, 9},
      {9, 0, 1, 0, 9, 6, 1, 0, 9, 9},
      {9, 0, 9, 0, 0, 3, 8, 0, 9, 9},
      {9, 1, 0, 0, 0, 9, 9, 5, 0, 9},
      {9, 5, 6, 0, 0, 7, 2, 3, 7, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 9, 0, 7, 0, 0, 0, 9},
      {9, 0, 4, 0, 1, 9, 0, 5, 0, 9},
      {9, 0, 3, 0, 9, 9, 0, 6, 0, 9},
      {9, 0, 1, 0, 9, 0, 0, 9, 0, 9},
      {9, 0, 4, 0, 5, 0, 0, 0, 0, 9},
      {9, 9, 9, 6, 3, 7, 0, 0, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 4, 0, 0, 0, 9},
      {9, 8, 0, 0, 0, 9, 0, 0, 0, 9},
      {9, 2, 0, 0, 7, 0, 9, 0, 0, 9},
      {9, 9, 0, 0, 9, 0, 9, 0, 0, 9},
      {9, 0, 0, 9, 0, 0, 2, 5, 0, 9},
      {9, 9, 7, 8, 5, 0, 9, 9, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 0, 4, 0, 0, 9},
      {9, 9, 0, 0, 0, 0, 9, 0, 0, 9},
      {9, 4, 0, 0, 0, 0, 0, 0, 9, 9},
      {9, 9, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 8, 0, 0, 6, 0, 0, 0, 0, 9},
      {9, 6, 0, 0, 4, 0, 4, 8, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      { 9, 0, 0, 0, 7, 8, 0, 0, 0, 9},
      { 9, 7, 0, 0, 8, 7, 0, 0, 7, 9},
      { 9, 6, 5, 0, 9, 9, 0, 5, 6, 9},
      { 9, 2, 1, 0, 9, 9, 0, 2, 1, 9},
      { 9, 3, 4, 0, 0, 0, 0, 4, 3, 9},
      { 9, 7, 8, 9, 7, 5, 9, 8, 7, 9},
      { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },    
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 9, 0, 0, 0, 9, 9},
      {9, 0, 3, 0, 9, 0, 0, 0, 2, 9},
      {9, 0, 9, 0, 0, 0, 0, 0, 9, 9},
      {9, 0, 0, 9, 0, 9, 8, 0, 9, 9},
      {9, 0, 0, 0, 8, 0, 4, 9, 0, 9},
      {9, 0, 0, 3, 9, 0, 2, 4, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 4, 3, 9, 0, 0, 0, 0, 0, 9},
      {9, 1, 7, 4, 5, 0, 0, 8, 0, 9},
      {9, 9, 5, 9, 9, 0, 6, 9, 0, 9},
      {9, 9, 7, 0, 0, 0, 9, 9, 0, 9},
      {9, 0, 9, 0, 0, 0, 9, 9, 0, 9},
      {9, 0, 3, 1, 6, 0, 8, 1, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 9, 3, 4, 0, 5, 9},
      {9, 6, 0, 0, 0, 9, 7, 0, 9, 9},
      {9, 9, 0, 0, 0, 7, 9, 0, 0, 9},
      {9, 0, 0, 0, 0, 9, 0, 0, 0, 9},
      {9, 0, 0, 3, 0, 0, 0, 0, 0, 9},
      {9, 0, 6, 4, 0, 5, 0, 0, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 4, 0, 4, 0, 0, 9, 0, 9},
      {9, 8, 9, 0, 6, 9, 0, 0, 0, 9},
      {9, 1, 0, 0, 9, 0, 0, 0, 0, 9},
      {9, 9, 0, 0, 0, 7, 0, 0, 0, 9},
      {9, 5, 8, 0, 0, 1, 0, 0, 0, 9},
      {9, 9, 1, 7, 1, 9, 6, 0, 5, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 7, 6, 0, 0, 0, 0, 0, 0, 9},
      {9, 9, 9, 2, 0, 0, 0, 9, 9, 9},
      {9, 0, 9, 9, 0, 0, 0, 1, 0, 9},
      {9, 0, 9, 0, 0, 0, 7, 9, 0, 9},
      {9, 0, 0, 0, 0, 1, 2, 0, 4, 9},
      {9, 0, 0, 0, 4, 2, 6, 0, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 7, 0, 0, 9, 9, 0, 6, 9},
      {9, 0, 9, 0, 0, 0, 4, 0, 5, 9},
      {9, 0, 9, 0, 0, 0, 9, 0, 9, 9},
      {9, 0, 0, 0, 0, 0, 0, 8, 1, 9},
      {9, 0, 0, 0, 0, 0, 6, 7, 9, 9},
      {9, 5, 0, 3, 8, 6, 4, 1, 3, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 9, 0, 6, 9, 0, 8, 9, 9},
      {9, 9, 0, 8, 7, 0, 0, 9, 0, 9},
      {9, 0, 0, 9, 9, 9, 0, 0, 9, 9},
      {9, 0, 0, 9, 9, 0, 0, 0, 9, 9},
      {9, 0, 0, 2, 0, 0, 0, 0, 9, 9},
      {9, 2, 0, 9, 9, 6, 0, 7, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 2, 8, 6, 9, 9, 0, 9},
      {9, 2, 3, 9, 1, 9, 0, 0, 0, 9},
      {9, 3, 7, 0, 5, 0, 0, 0, 0, 9},
      {9, 9, 9, 0, 9, 0, 3, 0, 0, 9},
      {9, 3, 1, 0, 3, 7, 5, 0, 4, 9},
      {9, 9, 2, 8, 2, 6, 3, 4, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 2, 0, 0, 1, 0, 8, 9, 0, 9},
      {9, 9, 0, 5, 9, 3, 2, 0, 0, 9},
      {9, 0, 0, 9, 0, 8, 9, 0, 0, 9},
      {9, 0, 0, 0, 0, 9, 0, 0, 3, 9},
      {9, 1, 0, 4, 0, 0, 2, 5, 6, 9},
      {9, 6, 0, 8, 0, 0, 8, 1, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 7, 6, 0, 6, 0, 4, 9},
      {9, 7, 0, 9, 5, 0, 9, 0, 6, 9},
      {9, 1, 5, 4, 6, 0, 0, 0, 7, 9},
      {9, 7, 2, 8, 2, 0, 6, 1, 9, 9},
      {9, 1, 8, 6, 9, 0, 4, 5, 3, 9},
      {9, 5, 6, 5, 2, 0, 3, 4, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 2, 9, 0, 0, 5, 4, 9},
      {9, 0, 6, 9, 0, 0, 0, 8, 9, 9},
      {9, 0, 9, 0, 0, 0, 0, 9, 0, 9},
      {9, 2, 6, 0, 0, 0, 7, 1, 3, 9},
      {9, 3, 8, 0, 8, 6, 3, 7, 5, 9},
      {9, 4, 9, 6, 9, 3, 1, 9, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 8, 0, 9, 8, 0, 3, 0, 1, 9},
      {9, 7, 0, 9, 1, 0, 9, 0, 2, 9},
      {9, 9, 0, 0, 8, 0, 0, 0, 4, 9},
      {9, 0, 0, 0, 9, 5, 0, 0, 9, 9},
      {9, 5, 3, 0, 8, 7, 2, 4, 9, 9},
      {9, 1, 2, 0, 5, 8, 1, 2, 7, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 5, 2, 9, 0, 0, 4, 9},
      {9, 0, 0, 6, 1, 0, 0, 0, 6, 9},
      {9, 0, 1, 9, 9, 0, 0, 0, 1, 9},
      {9, 0, 5, 9, 0, 0, 8, 6, 5, 9},
      {9, 0, 6, 0, 0, 0, 5, 2, 9, 9},
      {9, 1, 8, 9, 1, 6, 1, 4, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 3, 1, 6, 9, 9, 5, 9},
      {9, 0, 0, 9, 9, 9, 9, 0, 6, 9},
      {9, 3, 2, 0, 0, 0, 0, 0, 3, 9},
      {9, 9, 9, 6, 0, 7, 0, 0, 8, 9},
      {9, 8, 0, 1, 0, 1, 0, 0, 9, 9},
      {9, 9, 2, 4, 7, 5, 6, 4, 8, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 1, 0, 0, 7, 9, 0, 0, 0, 9},
      {9, 9, 0, 7, 5, 0, 0, 1, 0, 9},
      {9, 0, 0, 8, 9, 0, 4, 9, 0, 9},
      {9, 9, 0, 5, 0, 0, 9, 9, 2, 9},
      {9, 0, 0, 9, 0, 9, 0, 5, 4, 9},
      {9, 8, 4, 0, 7, 2, 6, 4, 6, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 2, 0, 0, 0, 0, 9, 9},
      {9, 0, 0, 9, 0, 0, 0, 0, 0, 9},
      {9, 1, 0, 9, 0, 0, 0, 0, 0, 9},
      {9, 9, 5, 0, 0, 2, 0, 0, 0, 9},
      {9, 0, 7, 8, 0, 9, 0, 0, 7, 9},
      {9, 1, 9, 3, 0, 8, 3, 0, 5, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 5, 0, 0, 0, 6, 0, 6, 2, 9},
      {9, 4, 8, 0, 0, 9, 0, 3, 7, 9},
      {9, 1, 7, 0, 0, 3, 0, 5, 4, 9},
      {9, 9, 5, 0, 7, 1, 3, 6, 9, 9},
      {9, 9, 6, 4, 3, 7, 6, 7, 6, 9},
      {9, 8, 4, 5, 9, 6, 2, 4, 7, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 5, 0, 9, 9, 9, 0, 0, 9},
      {9, 0, 9, 0, 0, 0, 0, 2, 0, 9},
      {9, 0, 9, 0, 0, 0, 0, 9, 5, 9},
      {9, 0, 6, 0, 0, 0, 0, 3, 6, 9},
      {9, 1, 7, 0, 0, 3, 0, 7, 9, 9},
      {9, 9, 9, 2, 0, 7, 5, 1, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 2, 4, 5, 0, 3, 9, 9},
      {9, 0, 0, 3, 9, 3, 8, 9, 9, 9},
      {9, 8, 4, 9, 9, 2, 1, 9, 0, 9},
      {9, 1, 3, 2, 0, 9, 2, 0, 0, 9},
      {9, 5, 9, 6, 0, 7, 8, 0, 6, 9},
      {9, 1, 8, 5, 3, 2, 1, 7, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 3, 0, 6, 9, 0, 9},
      {9, 0, 0, 0, 9, 0, 9, 0, 6, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 1, 9},
      {9, 8, 0, 0, 0, 3, 0, 0, 9, 9},
      {9, 7, 0, 0, 0, 4, 0, 7, 0, 9},
      {9, 9, 0, 4, 8, 9, 0, 1, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 0, 3, 0, 5, 0, 9},
      {9, 0, 0, 0, 0, 4, 0, 3, 0, 9},
      {9, 0, 8, 0, 9, 1, 3, 9, 9, 9},
      {9, 0, 4, 0, 0, 2, 9, 0, 5, 9},
      {9, 0, 3, 6, 0, 9, 9, 0, 1, 9},
      {9, 4, 2, 8, 6, 3, 5, 0, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 6, 0, 0, 9, 8, 0, 0, 0, 9},
      {9, 3, 0, 0, 0, 9, 0, 0, 0, 9},
      {9, 9, 0, 0, 0, 0, 0, 0, 5, 9},
      {9, 9, 0, 2, 7, 0, 0, 0, 1, 9},
      {9, 0, 5, 6, 4, 0, 0, 0, 9, 9},
      {9, 8, 3, 9, 7, 0, 1, 2, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 3, 4, 0, 6, 9, 8, 3, 9},
      {9, 0, 9, 3, 0, 9, 0, 3, 8, 9},
      {9, 0, 0, 8, 0, 4, 0, 7, 3, 9},
      {9, 2, 0, 3, 0, 6, 0, 3, 8, 9},
      {9, 4, 1, 2, 7, 1, 0, 5, 1, 9},
      {9, 6, 9, 5, 1, 3, 8, 6, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 6, 9, 3, 0, 0, 9},
      {9, 0, 0, 0, 3, 0, 9, 7, 0, 9},
      {9, 0, 0, 0, 8, 0, 0, 4, 0, 9},
      {9, 0, 0, 0, 9, 0, 0, 9, 0, 9},
      {9, 0, 0, 0, 0, 7, 2, 8, 0, 9},
      {9, 0, 0, 6, 8, 4, 9, 2, 8, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 1, 0, 0, 7, 0, 0, 0, 9},
      {9, 0, 5, 0, 5, 9, 0, 0, 0, 9},
      {9, 0, 8, 0, 9, 9, 0, 0, 2, 9},
      {9, 3, 9, 5, 1, 0, 0, 0, 6, 9},
      {9, 2, 3, 6, 3, 4, 0, 3, 7, 9},
      {9, 3, 7, 8, 4, 2, 0, 8, 3, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 9, 0, 0, 9, 0, 0, 0, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 0, 0, 0, 0, 5, 9, 9},
      {9, 0, 0, 9, 1, 0, 0, 9, 0, 9},
      {9, 0, 0, 0, 6, 0, 0, 0, 0, 9},
      {9, 4, 5, 6, 4, 8, 1, 8, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 7, 0, 9, 0, 9, 9, 9},
      {9, 0, 0, 4, 0, 0, 0, 0, 9, 9},
      {9, 0, 0, 9, 0, 0, 0, 0, 9, 9},
      {9, 0, 0, 0, 0, 0, 5, 9, 0, 9},
      {9, 0, 3, 8, 4, 0, 9, 8, 0, 9},
      {9, 8, 7, 5, 2, 8, 2, 3, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 3, 0, 2, 0, 3, 0, 3, 0, 9},
      {9, 9, 0, 9, 0, 9, 7, 9, 0, 9},
      {9, 9, 2, 0, 0, 0, 2, 0, 0, 9},
      {9, 7, 8, 0, 0, 0, 1, 3, 0, 9},
      {9, 2, 4, 0, 4, 1, 9, 4, 5, 9},
      {9, 9, 2, 4, 5, 8, 3, 2, 3, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 8, 0, 2, 0, 9, 0, 3, 0, 9},
      {9, 1, 0, 9, 6, 7, 0, 9, 0, 9},
      {9, 2, 0, 0, 7, 3, 0, 9, 0, 9},
      {9, 4, 0, 1, 4, 9, 4, 0, 0, 9},
      {9, 7, 5, 8, 6, 2, 5, 0, 0, 9},
      {9, 3, 1, 4, 2, 1, 3, 0, 3, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 1, 0, 1, 0, 0, 9},
      {9, 0, 1, 0, 9, 0, 9, 0, 0, 9},
      {9, 0, 9, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 0, 4, 0, 0, 0, 0, 9},
      {9, 0, 0, 7, 3, 0, 0, 0, 0, 9},
      {9, 0, 0, 6, 7, 3, 6, 0, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 0, 1, 0, 2, 9},
      {9, 3, 0, 0, 7, 0, 9, 0, 4, 9},
      {9, 7, 0, 0, 9, 0, 5, 1, 9, 9},
      {9, 9, 0, 0, 8, 0, 6, 9, 0, 9},
      {9, 8, 4, 0, 9, 0, 9, 0, 0, 9},
      {9, 5, 3, 0, 0, 0, 0, 6, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 2, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 9, 0, 0, 3, 0, 9, 9},
      {9, 4, 0, 0, 0, 0, 8, 0, 0, 9},
      {9, 9, 0, 8, 0, 4, 9, 0, 0, 9},
      {9, 0, 0, 1, 0, 9, 0, 0, 0, 9},
      {9, 0, 1, 2, 5, 3, 0, 0, 5, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 9, 0, 0, 0, 9, 3, 0, 9},
      {9, 0, 0, 4, 0, 0, 0, 9, 0, 9},
      {9, 0, 0, 1, 8, 0, 0, 0, 0, 9},
      {9, 9, 0, 9, 6, 0, 0, 0, 0, 9},
      {9, 3, 4, 0, 1, 0, 0, 0, 2, 9},
      {9, 9, 9, 7, 8, 7, 6, 2, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 7, 9, 0, 0, 9, 9, 9},
      {9, 0, 0, 9, 0, 0, 0, 0, 3, 9},
      {9, 0, 0, 0, 0, 0, 0, 9, 7, 9},
      {9, 0, 0, 0, 0, 0, 8, 1, 6, 9},
      {9, 0, 0, 2, 0, 0, 9, 9, 9, 9},
      {9, 0, 1, 3, 0, 6, 8, 7, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 3, 0, 9, 0, 0, 9, 0, 0, 9},
      {9, 9, 0, 9, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 4, 2, 7, 3, 0, 0, 0, 9},
      {9, 0, 3, 4, 6, 7, 8, 0, 8, 9},
      {9, 7, 6, 2, 7, 1, 3, 0, 1, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 0, 6, 0, 9, 0, 0, 9},
      {9, 3, 0, 0, 2, 0, 0, 5, 0, 9},
      {9, 7, 0, 0, 9, 0, 0, 1, 0, 9},
      {9, 4, 6, 0, 0, 4, 0, 3, 2, 9},
      {9, 9, 4, 0, 0, 3, 0, 9, 9, 9},
      {9, 1, 3, 0, 7, 5, 0, 3, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 0, 9, 0, 1, 9, 9},
      {9, 0, 0, 0, 0, 6, 0, 4, 6, 9},
      {9, 0, 0, 0, 0, 2, 0, 7, 4, 9},
      {9, 0, 8, 0, 2, 1, 8, 2, 9, 9},
      {9, 8, 9, 4, 1, 7, 3, 7, 3, 9},
      {9, 2, 8, 3, 5, 3, 5, 6, 4, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 0, 0, 0, 0, 0, 9, 9},
      {9, 0, 4, 0, 0, 0, 0, 8, 2, 9},
      {9, 0, 9, 0, 0, 0, 0, 9, 9, 9},
      {9, 9, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 0, 0, 1, 0, 5, 1, 8, 9},
      {9, 0, 3, 0, 3, 0, 4, 2, 5, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 5, 0, 0, 6, 0, 9, 0, 0, 9},
      {9, 1, 3, 6, 5, 0, 8, 0, 2, 9},
      {9, 4, 7, 4, 7, 0, 7, 0, 4, 9},
      {9, 8, 1, 6, 3, 0, 8, 0, 1, 9},
      {9, 4, 9, 2, 1, 0, 1, 0, 8, 9},
      {9, 9, 3, 7, 6, 0, 9, 8, 1, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 7, 0, 0, 9, 0, 0, 9, 9},
      {9, 0, 9, 0, 0, 1, 0, 0, 0, 9},
      {9, 0, 9, 0, 0, 9, 0, 0, 8, 9},
      {9, 0, 0, 0, 4, 0, 9, 0, 1, 9},
      {9, 0, 6, 8, 3, 5, 0, 0, 5, 9},
      {9, 1, 4, 6, 4, 3, 7, 1, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 8, 0, 3, 0, 9, 9, 0, 9},
      {9, 0, 6, 0, 4, 0, 0, 3, 0, 9},
      {9, 0, 8, 5, 9, 0, 0, 7, 0, 9},
      {9, 1, 9, 4, 1, 0, 0, 6, 0, 9},
      {9, 2, 0, 2, 6, 2, 0, 8, 0, 9},
      {9, 3, 0, 6, 2, 7, 4, 5, 6, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 0, 0, 0, 0, 5, 9},
      {9, 7, 2, 0, 0, 0, 0, 0, 9, 9},
      {9, 5, 6, 0, 0, 4, 0, 0, 0, 9},
      {9, 9, 9, 0, 9, 5, 0, 0, 6, 9},
      {9, 0, 0, 0, 0, 9, 0, 3, 4, 9},
      {9, 2, 9, 0, 0, 5, 3, 4, 7, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 0, 0, 0, 0, 0, 0, 9},
      {9, 0, 1, 0, 9, 0, 0, 0, 0, 9},
      {9, 0, 6, 0, 0, 7, 0, 0, 0, 9},
      {9, 0, 9, 0, 0, 9, 0, 0, 0, 9},
      {9, 6, 0, 0, 0, 2, 0, 0, 0, 9},
      {9, 1, 7, 0, 0, 5, 0, 5, 2, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 9, 9, 0, 0, 2, 9, 9},
      {9, 0, 0, 0, 6, 3, 0, 8, 0, 9},
      {9, 0, 0, 7, 9, 9, 0, 4, 9, 9},
      {9, 9, 0, 8, 7, 5, 0, 9, 0, 9},
      {9, 7, 6, 2, 8, 4, 0, 3, 0, 9},
      {9, 4, 7, 9, 5, 6, 0, 4, 0, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 0, 5, 0, 2, 1, 0, 8, 9},
      {9, 8, 0, 4, 0, 9, 9, 0, 9, 9},
      {9, 2, 0, 2, 0, 0, 9, 0, 2, 9},
      {9, 8, 5, 1, 9, 0, 0, 0, 9, 9},
      {9, 2, 6, 9, 4, 8, 0, 0, 4, 9},
      {9, 3, 8, 6, 8, 2, 3, 4, 8, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 6, 0, 0, 0, 0, 7, 0, 9, 9},
      {9, 7, 0, 0, 0, 0, 1, 0, 0, 9},
      {9, 9, 0, 9, 3, 8, 9, 0, 0, 9},
      {9, 0, 0, 6, 1, 9, 0, 2, 3, 9},
      {9, 6, 8, 4, 2, 4, 0, 5, 4, 9},
      {9, 9, 5, 7, 1, 6, 0, 1, 6, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 9, 0, 0, 0, 0, 9, 0, 8, 9},
      {9, 0, 0, 0, 0, 0, 6, 0, 4, 9},
      {9, 0, 0, 0, 0, 0, 2, 0, 9, 9},
      {9, 3, 0, 0, 0, 0, 9, 0, 0, 9},
      {9, 8, 0, 8, 0, 3, 5, 4, 6, 9},
      {9, 9, 6, 7, 0, 2, 7, 5, 9, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   },
   {
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9},
      {9, 0, 9, 0, 0, 2, 8, 0, 0, 9},
      {9, 0, 0, 0, 2, 9, 9, 0, 0, 9},
      {9, 9, 0, 0, 9, 7, 0, 0, 0, 9},
      {9, 0, 0, 0, 0, 1, 3, 0, 0, 9},
      {9, 1, 0, 0, 0, 9, 9, 0, 0, 9},
      {9, 7, 3, 8, 0, 9, 8, 0, 8, 9},
      {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
   }
};
#endif

void DumpLevels()
{
	Board board;
	board.SetSize(10, 8);

	for (int n = 0; n < 59; n++) {
		byte *pb = board.m_pb;
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 10; x++) {
				byte b = level[n][y][x];
				switch (b) {
				case 9:
					b = 0xff;
					break;

				case 0:
					b = 0;
					break;

				default:
					b += 'a' - 1;
					break;
				}
				*pb++ = b;
			}
		}
		//board.Print();
		board.PrintNotation();
		printf("\n");
	}
}
#endif


Board *gpboardList;
Board *gpboardFreeList;

void MakePath(Board *pboardSolved, MoveList *pmlPath)
{
//printf("Make Path:\n");

	int c = pboardSolved->GetStepCount();
	pmlPath->SetMoveCount(c);
	Board *pboardT = pboardSolved;
	for (int n = c - 1; n >= 0; n--) {
//pboardT->Print();
//printf("\n");
		Move mv;
		pboardT->GetStepMove(&mv);
		pmlPath->SetMove(n, &mv);
		pboardT = pboardT->m_pboardLast;
	}
}

void AddToList(Board *pboard)
{
	int nScore = pboard->GetScore();
	for (Board **ppboardT = &gpboardList; true; ppboardT = &(*ppboardT)->m_pboardNext) {
		if ((*ppboardT) == NULL || nScore < (*ppboardT)->GetScore()) {
			pboard->m_pboardNext = *ppboardT;
			*ppboardT = pboard;
			break;
		}
	}
}

void SearchAStar(Board *pboard, MoveList *pmlPath)
{
	dword msStart = GetTickCount();

	// Free from previous run

	FreeHashEntries();
	Board *pboardT = gpboardFreeList;
	while (pboardT != NULL) {
		Board *pboardNext = pboardT->m_pboardNext;
		delete pboardT;
		pboardT = pboardNext;
	}
	pboardT = gpboardList;
	while (pboardT != NULL) {
		Board *pboardNext = pboardT->m_pboardNext;
		delete pboardT;
		pboardT = pboardNext;
	}
	gpboardList = NULL;
	gpboardFreeList = NULL;

	// Add this board

	AddToHash(pboard->GetHashKey());

	Board *pboardLast = pboard;
	while (pboardLast != NULL) {
		MoveList *pml = pboardLast->GenerateMoves();

		for (int n = 0; n < pml->GetMoveCount(); n++) {
			// Perform the move

			Board *pboardNew = new Board;
			pboardNew->InitFrom(pboardLast);
			Move mv;
			pml->GetMove(n, &mv);
			pboardNew->DoMove(&mv);

#if 0
printf("Move %d\n", n);
pboardNew->Print();
printf("\n");
#endif

#if 0
printf("Move %d\n", n);
Board *pboardT = pboardNew;
while (pboardT != NULL) {
	pboardT->Print();
	printf("%d, %d, %d\n", pboardT->m_mvLast.x, pboardT->m_mvLast.y, pboardT->m_mvLast.nDir);
	printf("from:\n");
	pboardT = pboardT->m_pboardLast;
}
printf("\n");
#endif

			// Check to see if this position is already hashed. If so there is already
			// a path to this board

			if (IsHashed(pboardNew->GetHashKey())) {
				pboardNew->m_pboardNext = gpboardFreeList;
				gpboardFreeList = pboardNew;
				continue;
			}

			// If we have singles it's an invalid position (speed up)

			if (!pboardNew->IsSolvable() != 0) {
				pboardNew->m_pboardNext = gpboardFreeList;
				gpboardFreeList = pboardNew;
				continue;
			}

			// New position - add to list

			AddToHash(pboardNew->GetHashKey());
			AddToList(pboardNew);

			// Done?

			if (pboardNew->GetPieceCount() == 0) {
				MakePath(pboardNew, pmlPath);
				delete pml;
				return;
			}
		}
		delete pml;
		if (pboardLast != pboard) {
			pboardLast->m_pboardNext = gpboardFreeList;
			gpboardFreeList = pboardLast;
		}

		// Find the best board at this point

		pboardLast = gpboardList;
		if (pboardLast == NULL)
			break;
		gpboardList = pboardLast->m_pboardNext;

#if 0
printf("last board\n");
pboardLast->Print();
printf("\n");
#endif

		// Timeout?
//#ifdef _DEBUG
		if (GetTickCount() >= msStart + gcmsSolve)
			break;
//#endif
	}
}

void FixLevels(char *szFnPuzzles)
{
	FILE *pf = fopen(szFnPuzzles, "r");
	if (pf == NULL)
		ExitUsage();

	while (true) {
      char szSolve[256];
      int cMoves;
      int cPieces;

      if (fscanf(pf, "%d,%d,%s", &cPieces, &cMoves, szSolve) != 3) {
         if (feof(pf))
            break;
         printf("Fix: error reading puzzles\n");
         exit(1);
      }

		for (int i = 0; i < (int)strlen(szSolve); i++) {
			if (szSolve[i] == '\n') {
				szSolve[i] = 0;
				break;
			}
		}

		Board board;
		if (!board.Load(szSolve)) {
			printf("Bad Puzzle: %s\n", szSolve);
			continue;
		}

      // "Fix" board

      for (int y = 0; y < board.m_cy; y++) {
         for (int x = 0; x < board.m_cx; x++) {
            byte *pb = &board.m_pb[y * board.m_cx + x];
            if (*pb != 0 && *pb != 0xff) {
               (*pb)--;
               if (*pb < 'a') {
                  printf("Bad fix for some reason!\n");
                  exit(1);
               }
            }
         }
      }

      printf("%d,%d,", cPieces, cMoves);
      board.PrintNotation();
      printf("\n");
   }
   fclose(pf);
}

void ExitUsage()
{
	printf("Usage: vexsolv [options] [board]\n");
	printf("\t-file file specifies a file of puzzles to solve\n");
	printf("\t-seconds seconds specifies seconds to solve, default is 600 seconds\n");
	printf("\t-fill fills puzzle squares not used in the solution\n");
	printf("\t-nosolution does not print solution\n");
	printf("\t-notation prints notation only\n");
	printf("\t-quiet try to make shortened output\n");
	printf("\t-crossovercount print solution crossover count\n");
	printf("\t-movecount print solution move count\n");
	printf("\t-piececount print piece count of initial puzzle\n");
	printf("\t-score difficulty score of puzzle\n");
	printf("\t-makeini make an .ini file\n");
	printf("\t-gen=count generate levels\n");
	printf("\t-filter=a/b/c/d filter (a=cPcMin,b=cPcMax,c=ScoreMin,d=ScoreMax)\n");
	printf("\tboard: String representing the board, layed out by rows\n");
	printf("\tseparated by / character. Any characters represent pieces.\n");
	printf("\tThe ~ character represents emtpy, and a number represents a\n");
	printf("\tseries of non-moveable bricks. Board may be of any dimension.\n");
	printf("Example:\n");
	printf("\tvexsolv 10/10/3~~a~~2/3~~b~~2/3~ac~~2/3~bd~~2/3bdc~c2/10");
	exit(1);
}

int main(int argc, char **argv)
{
#ifdef INCL_DUMPLEVELS
	DumpLevels();
	return 0;
#endif

	char **ppsz = argv;
	int c = argc;

	bool fQuiet = false;
	bool fSolution = true;
	bool fNotation = false;
	bool fFill = false;
	bool fFile = false;
	bool fPuzzleCommandLine = false;
	bool fMoveCount = false;
	bool fPieceCount = false;
	bool fCrossoverCount = false;
	bool fScore = false;
	bool fFixLevels = false;
	bool fMakeIni = false;
	bool fGen = false;
	bool fFilter = false;
	gcmsSolve = 10 * 60 * 1000;
	char szSolve[256];
	char szFnPuzzles[_MAX_PATH];
	int cPcMin = -1;
	int cPcMax = -1;
	int nScoreMin, nScoreMax;
	int cLevelGen;

	ppsz++;
	c--;
	while (c-- != 0) {
		char *psz = *ppsz++;
		if (strcmp(psz, "-file") == 0) {
			psz = *ppsz++;
			c--;
			strcpy(szFnPuzzles, psz);
			fFile = true;
			continue;
		} else if (strcmp(psz, "-seconds") == 0) {
			psz = *ppsz++;
			c--;
			gcmsSolve = atoi(psz) * 1000;
			continue;
		} else if (strcmp(psz, "-fill") == 0) {
			fFill = true;
			continue;
		} else if (strcmp(psz, "-nosolution") == 0) {
			fSolution = false;
			continue;
		} else if (strcmp(psz, "-notation") == 0) {
			fNotation = true;
			continue;
		} else if (strcmp(psz, "-quiet") == 0) {
			fQuiet = true;
			continue;
		} else if (strcmp(psz, "-movecount") == 0) {
			fMoveCount = true;
			continue;
		} else if (strcmp(psz, "-piececount") == 0) {
			fPieceCount = true;
			continue;
		} else if (strcmp(psz, "-crossovercount") == 0) {
			fCrossoverCount = true;
			continue;
		} else if (strcmp(psz, "-score") == 0) {
			fScore = true;
			continue;
		} else if (strcmp(psz, "-fixlevels") == 0) {
			fFixLevels = true;
			continue;
		} else if (strcmp(psz, "-makeini") == 0) {
			fMakeIni = true;
			continue;
		} else if (strncmp(psz, "-gen=", 5) == 0) {
			fGen = true;
			int cScanned = sscanf(psz, "-gen=%d", &cLevelGen);
			if (cScanned != 1) {
				printf("Error parsing -gen= command.\n");
				ExitUsage();
			}
			continue;
		} else if (strncmp(psz, "-filter=", 8) == 0) {
			// -filter=a/b/c/d/e filter levels (a=cPcMin,b=cPcMax,c=ScoreMin,d=ScoreMax)
			fFilter = true;
			int cScanned = sscanf(psz, "-filter=%d/%d/%d/%d", &cPcMin, &cPcMax, &nScoreMin, &nScoreMax);
			if (cScanned != 4) {
				printf("Error parsing -filter= command.\n");
				ExitUsage();
			}
			continue;
		}
		if (c == 0) {
			// Puzzle on command line

			fPuzzleCommandLine = true;
			strcpy(szSolve, psz);
			break;
		}
	}

	// Randomize

	RandInit();

	if (!fPuzzleCommandLine && !fFile && !fGen) {
		printf("No puzzles to solve specified.\n");
		ExitUsage();
	}

   // Fix bug in the output of the level generator so we have 8 pieces,
   // not 9! Level generator script has been fixed, so this just fixes
   // existing generated levels.

   if (fFixLevels) {
      FixLevels(szFnPuzzles);
      return 0;
   }

   // Make ini file?

   if (fMakeIni) {
      fQuiet = true;
      fSolution = false;
      fPieceCount = false;
      fMoveCount = false;
      fNotation = false;
   }

   // Init hash table

	AllocHashTables(50, 50);

	// Read from file

	FILE *pf = NULL;
	if (fFile) {
		pf = fopen(szFnPuzzles, "r");
		if (pf == NULL)
			ExitUsage();
	}

	int cPuzzlesSolved = 0;
	int nPuzzle = 0;
	while (true) {
		Board board;
		if (fGen) {
			board.RandomGen(10, 8, cPcMin, cPcMax, -1);
		} else {
			if (fFile) {
				if (fgets(szSolve, sizeof(szSolve), pf) == NULL)
					break;
			}
			for (int i = 0; i < (int)strlen(szSolve); i++) {
				if (szSolve[i] == '\n') {
					szSolve[i] = 0;
					break;
				}
			}

			if (!board.Load(szSolve)) {
				printf("Bad Puzzle: %s\n", szSolve);
				continue;
			}
		}

		nPuzzle++;
		board.InitCounts();

		// Filtering piece counts?
		
		if (fFilter) {
			int cPieces = board.GetPieceCount();
			if (cPieces < cPcMin || cPieces > cPcMax)
				continue;
		}

		if (!fQuiet) {
			printf("\n");
			printf("Board:\n");
			board.Print();
			printf("\n");	
		}

		MoveList mlPath;
		SearchAStar(&board, &mlPath);
		if (mlPath.GetMoveCount() != 0)
			cPuzzlesSolved++;

		// If the level is filtered, see if it's what we want

		if (fFilter) {
			if (mlPath.GetMoveCount() == 0)
				continue;
			int nScore = board.CalcDifficultyScore(&mlPath);
			if (nScore < nScoreMin || nScore > nScoreMax)
				continue;
		}

		if (fFill && mlPath.GetMoveCount() != 0) {
			board.FillUnused(&mlPath);

			if (!fQuiet) {
				printf("\n");
				printf("Filled Board:\n");
				board.Print();
				printf("\n");
			}
		}

		if (fMakeIni) {
			//printf("[Level \"Level %d\"]\n", cPuzzlesSolved - 1);
			printf("[Level]\n");
			printf("board=");
			board.PrintNotation();
			printf("\n");
			if (mlPath.GetMoveCount() != 0) {
				//printf("moves=%d\n", mlPath.GetMoveCount());
				printf("solution=");
				mlPath.Print();
			}
			printf("\n");
		}

		if (fSolution) {
			printf("Puzzle %d solution is %d moves:\n", nPuzzle, mlPath.GetMoveCount());
			if (fQuiet) {
				mlPath.Print();
				printf("\n");
			} else {
				int cMoves = mlPath.GetMoveCount();
				Board boardT;
				boardT.InitFrom(&board);
				for (int n = 0; n < cMoves; n++) {
					boardT.Print();
					Move mv;
					mlPath.GetMove(n, &mv);
					printf("(%d,%d)%c\n\n", mv.x, mv.y, mv.nDir < 0 ? 'L' : 'R');
					boardT.DoMove(&mv);
				}
				board.Print();
				mlPath.Print();
				printf("\n");
			}
		}

		if (mlPath.GetMoveCount() != 0) {
			if (fCrossoverCount) {
				int cMoves = mlPath.GetMoveCount();
				Board boardT;
				boardT.InitFrom(&board);
				for (int n = 0; n < cMoves; n++) {
					Move mv;
					mlPath.GetMove(n, &mv);
					boardT.DoMove(&mv);
				}
				printf("%d,", boardT.GetCrossoverCount());
			}

			if (fScore)
				printf("%d,", board.CalcDifficultyScore(&mlPath));

			if (fPieceCount)
				printf("%d,", board.GetPieceCount());

			if (fMoveCount)
				printf("%d,", mlPath.GetMoveCount());

			if (fNotation)
				board.PrintNotation();

         if (fScore || fCrossoverCount || fPieceCount || fMoveCount || fNotation)
			   printf("\n");
		}

		if (fGen) {
			cLevelGen--;
			if (cLevelGen <= 0)
				break;
		} else {
			if (!fFile)
				break;
		}
	}
	if (fFile)
		fclose(pf);

	// Free hash tables

	FreeHashTables();

	return 0;
}

#if 0
	// Search for solutions

	MoveList mlPath;
	bool fStop = false;
	for (int plyMax = 1; !fStop; plyMax++) {
		printf("Iteration %d.\n", plyMax);
		board.Search(&mlPath, 0, plyMax, &fStop);
	}
#endif