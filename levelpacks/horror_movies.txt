Abominable Dr. Phibes
After Midnight
Aileen Wuornos:  A Selling of a Serial Killer (documentary)
Alice Sweet Alice
Alien
Aliens
Alien3
Alien Resurrection
Alien Prey
Alison's Birthday
All American Murder (?)
Alligator
Alone in the Dark
American Psycho
American Werewolf in London
Amityville :  A New Generation
Amityville 1992:  It's About Time
Amityville Horror
Amityville II:  The Possession
Amityville III:  The Demon
Amityville IV:  The Evil Escapes
And Now the SCREAMing Starts
And Then There Were None
Andy Warhol's Dracula
Andy Warhol's Frankenstein
April Fool's Day
Arachnophobia
Arnold
Astro-Zombies
Asylum
Atom Age Vampires
Attack of the Crab Monsters
Attack of the Giant Leeches
Attack of the Swamp Creature
Audrey Rose
The Addiction
The Alpha Incident
The Ape Man
The Ape
The Attic
The Awakening
Basket Case
Bear Island
The Beast From 20,000 Fathoms
Beast From Haunted Cave
The Beast in the Cellar
The Beast Must Die
Beast of the Dead
The Beast Within
Bedlam
The Bees
Before I Hang
The Being
Ben
Berserk
Beware!  The Blob
Beware, My Lovely
Beyond Evil
Beyond the Door
Beyond the Door 2
Beyond the Living
Billy the Kid VS Dracula
Bird Man of Alcatraz
The Birds
Bishop Murder Case
The Black Room
Black Sabbath
The Black Scorpion
Blackenstein
Blacula
Blind Date
The Blob
Blood and Black Lace
Blood and Lace
Blood and Roses
Blood Bath
Blood Beach
Blood Beast
Blood Feast
Blood From the Mummy's Tomb
Blood Legacy
Blood mania
Blood of Dracula
Blood of Dracula's Castle
Blood of Ghastly Horror
Blood on Satan's Claw
Bloodeaters
Bloodline
Blood-spattered Bride
The Bloodsuckers
Bloodthirsty Butchers
Bloodtide
Bloody Birthday
Bloody Mama
Blow Out
Blue Sunshine
Bluebeard
Body Double
Body Parts
The Body Snatcher
The Body Snatchers
The Bog
The Boogeyman II
The Boogeyman
The Boogie Man Will Get You
Bordello of Blood
The Boston Strangler
Boy Who Cried Werewolf
The Boys From Brazil
Bram Stoker's Dracula
Bride of Chucky
Bride of Frankenstein
Bride of the Monster
The Bride
The Brighton Strangler
Brimstone and Treacle
The Brood
Brotherhood of Satan
The Brute Man
Bucket of Blood
Bug
Buried Alive
Burn, Witch, Burn!
The Burning
Burnt Offerings
The Baby
C.H.U.D
C.H.U.D.  2-Bud the Chud
The Cabinet of Dr.  Calgari
Caged Fury
Camp on Blood Island
Candyman
Candyman:  Farewell to the Flesh
Cannibal Girls
The Cantervilled Ghost
Cape Canaveral Monsters
Cape Fear
Captain Kronos:  Vampire Hunter
Carnival of Souls
Carpathian Eagle
Carrie
Cars the Eat People
Castle of Evil
The Castle of Terror
Castle of the Living Dead
The Cat and the Canary
The Cat Creature
The Cat Creeps
Cat o' Nine Tails
Cat People (original)
Cat People (remake)
Cat's Eye
Cauldron of Blood
Cemetary Man
Chamber of Horrors
The Changeling
Charlie Boy
Children of the Corn
Children of the Corn II- The Final Sacrifice
Children of the Corn III- Urban Harvest
Children of the Corn IV:  The Gathering
Children of the Damned
Children of the Full Moon
Children of the Night
Children Shouldn't Play With Dead Things
The Children
Child's Play
Child's Play II
Child's Play III
Christine
Christmas Evil
Circus of Horrors
City of the Walking Dead
Class of 1984
The Collector
Color Me Blood Red
Coma
The Comedy of Terrors
Conqueror Worm
The Corpse Vanishes
The Cosmic Monsters
Cauldron of Blood
Count Dracula
Count Dracula and His Vampire Bride
Count Yorga, Vampire
Countess Dracula
The Craft
The Crater Lake Monster
The Crawling Eye
The Crawling Hand
Creature From Black Lake
Creature From the Black Lagoon
Creature From the Haunted Sea
Creature of Destruction
Creature of the Walking Dead
Creepers
The Creeping Flesh
The Creeping Terror
Creepshow
Creepshow II
Critters
Critters II
Critters III
Critters IV
Cronos
Crucible of Horror
Cruise into Terror
Cujo
The Curse of Frankenstein
The Curse of King Tut's Tomb
The Curse of the Cat People
Curse of the Demon
Curse of the Living
Curtains
The Curve
The Cyclops
Damien:  Omen II
Dance of the Damned
Dangerously Close
Dark Forces
The Dark Half
The Dark Mirror
Dark Places
The Dark Secret of Harvest Home
The Dark
Daughter of Dr.  Jekyll
Daughters of Satan
Dawn of the Dead
Day Mars Invaded Earth
Day of the Animals
Day of the Dead
Day of Wrath
Dead Alive
Dead and Buried
The Dead are Alive
Dead Calm
Dead Heat
Dead Man Walk
Dead Men Walk
Dead of Night
The Dead Zone
The Deadly Bees
Deadly Blessing
Deadly Eyes
Deadly Games
The Deadly Mantis
Deadly Sanctuary
The Deadly Tower
Dear Dead Delilah
Death at Love House
Death on the Diamond
Death Ship
Death Valley
Deathdream
The Deathmaster
Deathtrap
Deep End
Deep Red
Defenseless
Dementia 13
Demon
The Demon Lover
Demon of the Mind
Demon Seed
Demonic Toys
Demonoid, Messenger of Death
Demons
Demons II
The Demons of Ludlow
The Demonsville Terror
The Demonsville Terror
Deranged
Destination Inner Space
The Devil Bat
Devil Bat's Daughter
The Devil Commands
Devil Dog:  The Hound of Hell
The Devil Doll
Devil Girl from Mars
Devil Times 5
The Devil Within Her
The Devil's Cargo
The Devil's Own
The Devil's Rain
The Devil's Undead
The Devils
Diabolique
Dial M for Murder
Diary of a Madman
Die SCREAMing Mariane
Die!  Die!  My Darling!
Die, Monster, Die!
Dirty Hands
Disembodied
Diva
The Doctor and the Devils
Dominique is Dead
Don't Answer the Phone
Don't Be Afraid of the Dark
Don't Look in the Basement
Don't Look Now
Doppelganger
Dorian Gray
The Dorm That Dripped Blood
Double Exposure (?)
Dr.  Jekyll and Mr. Hyde
Dr.  Phibes Rises Again
Dr.  Terror's House of Horrors
Dracula (original)
Dracula (remake)
Dracula vs Frankenstein
Dracula's Daughter
Dracula's Dog
Dracula's Son
Dream Lover
Dressed to Kill
The Drifter
Duel
The Dunwich Horror
Earth vs.  The Flying Saucers
Earth vs. The Spider
Eaten Alive
Embryo
Empire of the Ants
Endless Night
The Entity
Equinox (The Beast)
Eraserhead
Evil Dead II
The Evil Dead
Evil of Frankenstein
The Evil
Evilspeak
Exorcist II, The:  The Heretic
The Exorcist III
The Exorcist
Experiment in Terror
Exposed
The Eye Creatures
Eyes of a Stranger
Eyes of Hell
The Eyes of Laura Mars
Eyewitness
Face of Marble
The Faculty
Fade to Black
Fall of the House of Usher
Family Plot
The Fan
Fatal Attraction
Fatal Games
Fear in the Night
Fear No Evil
The Fearless Vampire Killers
Fer-De-Lance
Fiend Without a Face
The Fifth Floor
The Final Conflict
Final Destination
Final Exam
The Final Terror
Fire!
Firestarter
First Man Into Space
The Flesh Eaters
Fleshburn
Flood!
The Fly II
Fly, The (Original)
Fly, The (Remake)
Fog Island
The Fog
Forbidden World
Foreign Correspondent
The Forest
The Fourth Man
Frankenstein (Original)
Frankenstein (Remake)
Frankenstein (Restored Version)
Frankenstein and the Monster from Hell
Frankenstein Conquers the World
Frankenstein General Hospital
Frankenstein Island
Frankenstein meets the Wolf Man
Frankenstein-1970
Frankenstein's Daughter
Freaks
Freddy's Dead:  The Final Nightmare
Frenzy
Friday the 13th
Friday the 13th Part VIII:  Jason Takes Manhattan
Friday the 13th VI:  Jason Lives
Friday the 13th:  Part III
Friday the 13th:  Part V- A New Beginning
Friday the 13th:  Part VII:  The New Blood
Friday the 13th:  The Final Chapter
Friday the 13th: Part II
Fright
Fright Night
Fright Night
Fright Night II
The Frightened Bride
Frogs
From Beyond
From Beyond the Grave
From Hell it Came
The Funhouse
The Fury
Galaxy of Terror
Game of Death
Gammura, The Invincible
The Garden Murder Case
Gaslight
Gaurdian of the Abyss
The Gaurdian
Ghidrah, The Three-headed Monster
The Ghost Breaker
Ghost Brigade
The Ghost of Frankenstein
Ghost Story
The Ghost
The Ghoul
Ghoulies
The Giant Gila Monster
The Giant Spider Invasion
Gigantis, The Fire Monster
Godzilla 1985
Godzilla vs Monster Zero
Godzilla vs Mothra
Godzilla, The King of the Mosters
Gorgo
The Gorgon
Gothic
Graduation Day
Grave of the Vampire
Gremlins
Gremlins II:  The New Batch
The Grim Reaper
Grizzly
H20
Halloween
Halloween II
Halloween III:  Season of the Witch
Halloween IV:  The Return of Michael Myers
Halloween V
Halloween: The Curse of Michael Myers
The Hand the Rocks the Cradle
The Hand
Hands of the Ripper
Hangover Square
Happy Birthday to Me
Hatchet for the Honeymoon
Haunted Honeymoon
The Haunted Palace
The Haunted Strangler
The Haunted
The Haunting
Haunts
He Knows You're Alone
He Walked By Night
Hear No Evil
The Hearse
Hell Knight
Hell Night
Hellbound:  Hellraiser II
Hello Mary Lou:  Prom Night III
Hellraiser
Hellraiser III:  Hell on Earth
Hellraiser:  Bloodline
Henry:  Portrait of a Serial Killer
Henry:  Portrait of a Serial Killer II
The Hideous Sun Demon
The Hills Have Eyes
The Hitcher
Holocaust 2000
Holy Terror
Homebodies
Homicidal
The Horrible Dr. Hitchcock
Horror Castle
The Horror Chamber of Dr. Faustus
Horror Express
Horror Hospital
Horror Hotel
Horror Island
Horror of Dracula
Horror of Frankenstein
The Horror of Party Beach
Horror on Snape Island
The Horror Show
Horrors of the Black Museum
House
House II:  The Second Story
House IV
The House of  Seven Corpses
House of Evil
The House of Exorcism
The House of Fear
House of Frankenstein
House of Horrors
House of the Dead
House of the Long Shadows
House of Usher
House of Wax
House on Haunted Hill
The House on Skull Mountain
House on Sorority Row
The House that Bled to Death
The House that Dripped Blood
The House Where Evil Dwells
How to Make a Monster
Howling II, The:  Your Sister is a Werewolf
Howling III
Howling IV:  The Original Nightmare
Howling V:  The Rebirth
Howling VI- The Freaks
The Howling
The Human Factor
The Human Monster
Humanoids From the Deep
Humongous
The Hunger
The Hunting Party
Hush...Hush, Sweet Charlotte
The Ice Cream Man
I Confess
I Dismember Mama
Idle Hands
I Spit on Your Grave
I Walked With a Zombie
Impulse
In the Shadow of Kilimanjaro
The Incubus
Indestructible Man
The Initiation of Sarah
I Know What You Did Last Summer
The Intenecine of Sarah
Interview with the Vampire
Island Claws
The Island of Dr. Moreau
The Island of Lost Souls
The Island
Isle of the Dead
I Still Know What You Did Last Summer
It Came From Beneath the Sea
It Lives Again
It's Alive!
I Drink Your Blood
I Eat Your Skin
I Married a Monster From Outer Space
I, Monster
In Cold Blood
The Incredible Two-Headed Transplant!
Incredibly Strange Creatures Who Stopped Living and Became Mixed Up Zombies
Inn of the Damned
In the Mouth of Madness
Invasion of the Animal People
Invasion of the Bee Girls
Invasion of the Saucer People
The Invisible Creature
I Saw What You Did
Island of Terror
Island of the Burning Doomed
Isle of the Dead
It
It Came From Outer Space
It Lives Again
It's Alive III: Island of the Alive
It!  The Terror From Beyond Space
I Walked with a Zombie
I Was a Teenage Frankenstein
I Was a Teenage Werewolf
I Was a Teenage Zombie
Jack Frost
Jack the Ripper
Jack's Back
Jason Goes to Hell:  The Final Friday
Jaws
Jaws II
Jaws III
Jaws of Satan
Jaws the Revenge
Jennifer
Jesse James Meets Frankenstein's Daughter
The Jigsaw Man
Journey Into Fear
The Keep
The Keeper
Killer Fish
The Killer Inside Me
Killer Klowns From Outer Space
The Killer Shrews
The Killing Hour
King Kong (Original)
King Kong (Remake)
King of the Zombies
Kingdom of the Spiders
Kiss of the Tarantula
The Lady and the Monster
Lady Dracula
Lady Frankenstein
Lady in a Cage
The Lady Vanishes
Laserblast
The Last Horror Film
Last House on the Left, Part II
The Last House on the Left
The Last Wave
Leatherface:  Texas Chainsaw Massacre III
The Legacy
Legend of Boggy Creek
The Legend of Boggy Creek
Legend of Hell House
Legend of the Werewolf
Leprechaun
Leprechaun II
Leprechaun III
Leprechaun IV
Let's Kill Uncle
Let's Scare Jessica to Death
The Lift
Link
Lisa and the Devil
The Little Shop of Horrors
The Living Ghost
The Lodger
The Lost Boys
Macabre
Mad Doctor of Blood Island
The Mad Magician
The Mad Monster
Madhouse
The Magnetic Monster
Man Made Monster
Man With Two Heads
The Mangler
Maniac
Mansion of the Doomed
Mark of the Vampire
Martin
Mary Shelley's Frankenstein
The Masque of the Red Death
Massacre at Central High
Mauaoleum
Maximum Overdrive
Microwave Massacre
Monkey Shines, An Experiment in Fear
The Monster Club
Monster Dog
Monster from Green Hill
The Monster From the Ocean Floor
Monster in the Closet
The Monster Maker
The Monster of Piedras Blancas
Monster on the Campus
The Monster That Challenged the World
Monster Zero
Moon of the Wolf
Mortuary
Most Dangerous Game,  The
Motel Hell
Mother's Day
Multiple Maniacs
Mummy, The (Original)
Mummy, The (Remake)
The Mummy's Curse
The Mummy's Ghost
The Mummy's Hand
The Mummy's Shroud
The Mummy's Tomb
Mumsy, Nanny, Sonny, and Girly
Murder by Phone
Murder in Texas
The Murder Mansion
Murders in the Rue Morgue
Murders in the Zoo
Mutant
The Mutations
My Best Friend is a Vampire
My Bloody Valentine
My Sister, My Love
Mystery of the Wax Museum
The Naked Face
Navy vs. the Night Monsters
Near Dark
Needful Things
The Nest
The Nesting
Never Talk to Strangers
The New Kids
New Year's Evil
Night Caller
Night Caller From Outer Space
Night Creature
Night Creatures
Night Gallery
Night Must Fall
Night of Dark Shadows
Night of the Blood Beast
Night of the Demon
Night of the Demons
Night of the Ghouls
Night of the Howling Beast
Night of the Lepus
Night of the Living Dead
Night of the Zombies
The Night Visitor
Night Walker
Night Warning
Nightbreed
The Nightcomers
Nightfall
Night Flier
Nightflyers
Nightmare
Nightmare Castle
Nightmare in Wax
Nightmare on Elm Street III: Dream Warriors, A
Nightmare on Elm Street IV: The Dream Master, A
Nightmare on Elm Street Part II:  Freddy's Revenge
Nightmare on Elm Street V:  The Dream Child, A
Nightmare on Elm Street, A
Nightmares
The Nightstalker
Nightwing
Nightwish
976 Evil
No Place Like Homicide!
Nocturna
Nomads
Nosferatu
Nosferatu the Vampyre
Notorious
Number One Fan
The Oblong Box
Obsession
Octaman
Office Killer
Of Unknown Origin
The Omen
One Dark Night
Orca
The Obsessed
Old Dracula
One Frightened Night
The Pack
Paranoic
Parasite
Patrick
Peeping Tom
Pet Sematary
Pieces
The People Under the Stairs
Phantasm
Phantasm II
Phantasm III
The Phantom Creeps
Phantom From Outer Space
Phantom of the Opera
Phantoms
Pieces
Piranha
Piranha Part II:  The Spawning
The Pit and the Pendulum
Pitch Black
The Plague of the Zombies
Planet of Blood
Planet of the Vampires
Planets Against Us
Play Misty for Me
The Plumber
Poltergeist
Poltergeist II:  The Other Side
Poltergeist III
Possesion
The Power
The Premature Burial
The Prey
Prom Night
Prom Night III:  The Last Kiss
Prom Night IV:  Deliver Us From Evil
Prophecy
Psycho
Psycho Circus
Psycho II
Psycho III
Psycho IV:  The Beginning
Psychomania
The Psychopath
Psychosisters
Pulse
The Puma Man
Pumpkinhead
The Puppet Masters
Puppetmaster
Puppetmaster II
Puppetmaster III:  Toulan's Revenge
Purple Death From Outer Space
Pyro
The Pyx
Q
Rabid
Race with the Devil
Rage in Heaven
Raggedy Man
The Rats Are Coming!  The Werewolves are Here!
The Raven and the Black Cat
Raven Dance
The Raven
Raw Meat
Razorback
Re-Animator
The Red House
Reign of Terror
The Reincarnation of Peter Proud
Relentless
The Relic
The Reptile
Reptilicus
Retribution
Return From the Past
Return of Count Yorga
Return of Dr. X
The Return of Dracula
The Return of the Alien's Deadly Spawn
The Return of the Fly
Return of the Living Dead Part II
Return of the Living Dead Part III
A Return to Salem's Lot
The Return of the Living Dead
Return of the Swamp Thing
The Return of the Vampire
Revenge
Revenge of Frankenstein
Revenge of the Creature
Revenge of the Dead
Revenge of the Zombies
The Ripper
Road Games
The Road Killers
Robot Monster
Rollercoaster
Rope
Rosemary's Baby
Ruby
Rude Awakening
Run Stranger Run
Sabotage
The Sadist
Salem's Lot
Satan's Cheerleaders
Satan's School for Girls
Saturday the 14th
Savage Attraction
Savage Weekend
Savages
Scalpel
The Scarecrow
Scared Stiff
Scared to Death
Scars of Dracula
Scary Movie
Scenes From a Murder
Schizoid
SCREAM
SCREAM and SCREAM Again
SCREAM 2
SCREAM 3
SCREAM, Blacula, SCREAM
SCREAMers
Se7en
Secret Rapture
See No Evil
Seedpeople
Seeds of Evil
The Sender
The Sentinel
Serial Mom
The Seven Brothers Meet Dracula
The Severed Arm
Shadow of Evil
Shadow of the Cat
She Devil
The She-Beast
She-Wolf of London
The Shining
Shock
Shock Corridor
Shock Waves
Shocker
The Shout
Shrunken Heads
Signpost to Murder
The Silence of the Hams
The Silence of the Lambs
Silent Night, Bloody Night
Silent Night, Deadly Night
Silent Night, Deadly Night Part II
Silent Night, Deadly Night Part III
Silent Night, Deadly Night Part IV
Silent Night, Deadly Night Part V:  The Toymaker
Silent Rage
Silent SCREAM
Silver Bullet
Single White Female
Sinister Invasion
Sisters
Skeeter
The Skull
Skullduggery
Slaughter of the Vampires
Sleepy Hollow
Sleepwalkers
Slithis
Sole Survivor
Something Evil
Son of Blob
Son of Godzilla
The Son of Kong
Sorority House Massacre
Sorry, Wrong Number
Soul of a Monster
The Sound of Horror
Spasms
Spider Baby
The Spider
The Spiral Staircase
Spirits of the Dead
Splatter University
Squirm
The Stepford Wives
Stephen King's Nightshift Collection
Strait-Jacket
Strange Behaviour
The Strangeness
The Strangler
Student Bodies
The Stuff
Subspecies
Suspiria
Swamp Thing
The Swarm
Sweet 16
Tainted Blood
Tale of a Vampire
Tales From the Crypt
Tales From the Crypt presents Bordello of Blood
Tales From the Crypt: Demon Knight
Tales From the Darkside, The Movie
Tales From the Darkside, Volume I
Tales From the Hood
Tales of Terror
Tall, Dark, and Deadly
Tarantula
Targets
Taste of Blood, A
Teaching Mrs. Tingle
Teen Wolf
Teen Wolf, Too
Tentacles
The Terminal Man
Terrified
Terror at Red Wolf Inn
Terror at the Opera
Terror Creatures from the Grave
Terror in the Aisles
Terror in the Haunted HOuse
Terror in the Swamp
Terror in the Wax Museum
Terror out of the Sky
Terror Train
Terror Vision
The Terror
The Texas Chainsaw Massacre II
The Texas Chainsaw Massacre
Texas Chainsaw Massacre:  The Next Generation
Theater of Blood
Theater of Death
Them!
There's Nothing Out There
The Thing
The Thirsty Dead
13 Ghosts
Though Shalt Not Kill...Except
Three on a Meathook
The Ticket of Leave Man
To All a Good Night
To Die For
To Die For II: Son of  Darkness
To the Devil, a Daughter
The Tommyknockers
Torment
Torture Chamber of Baron Blood
The Torture Chamber of Dr. Sadism
Torture Dungeon
Torture Garden
Tourist Trap
The Town That Dreaded Sundown
Tracks
Trauma
Tremors
Tremors II:  Aftershocks
Trilogy of Terror
Trilogy of Terror II
Troll
Troll II
The Turn of the Screw
Twice-Told Tales
Twins of Evil
Twisted
Twisted Nightmare
Twisted Tales
Twitch of the Death Nerve
Two Evil Eyes
2,000 maniacs
The Uncanny
The Unseen
The Undying Monster
The Unearthly
The Uninvited
Unknown Island
The Unknown Terror
Unman, Wittering and Zigo
Unsane
The Unsuspected
Urban Legend
Urban Legend 2: The Final Cut
Vampire Circus
Vampire$
Vampyr
Vampyr
Varan, The Unbeleivable
Vault of Horror
Village of the Damned (Original)
Village of the Damned (Remake)
The Watcher in the Woods
What Ever Happened to Baby Jane
When a Stranger Calls
Who Slew Auntie Roo?
Wolfen
Wacko
War of the Zombies
The Wasp Woman
Web of the Spider
The Werewolf
Werewolf in the Girl's Dormitory
Werewolf in London
Wes Craven's New Nightmare
Witchboard
Witchcraft
Witchery
The Witchmaker
The Wolf Man
X the Unknown
Xtro
Xtro II:  The Second Encounter
You'll Find Out
The Young Poisoner's Handbook
Zombie
Zombie High
Zombie Island Massacre
Zombies of Mora Tau
Zontar, The Thing From Venus